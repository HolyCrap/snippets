(ns for-clojure)

(defn f 
  [pred coll]
  (let [map-with-predicate (fn [elem] {(pred elem) elem})
        initial {true '() false '()}
        collect (fn [m elem] (conj (m (pred elem)) elem))]
    
  (->> coll (map map-with-predicate) (reduce collect initial))))
 