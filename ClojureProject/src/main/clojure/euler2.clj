(ns euler2 (:require utils))

;number letter counts
(def digit-strings {1 "one" 2 "two" 3 "three" 4 "four" 5 "five" 6 "six" 7 "seven" 8 "eight" 9 "nine"})
(def teens {0 "ten" 1 "eleven" 2 "twelve" 3 "thirteen" 4 "fourteen" 5 "fifteen" 6 "sixteen" 7 "seventeen" 8 "eighteen" 9 "nineteen"})
(def decis {2 "twenty" 3 "thirty" 4 "forty" 5 "fifty" 6 "sixty" 7 "seventy" 8 "eighty" 9 "ninety"})

(defn parse-to-string [number]
  (if (= number 1000)
    "onethousand"
    (let [sb (new StringBuilder)
          hundreds (quot number 100)
          tens (quot (rem number 100) 10)
          ones (rem number 10)]
      (if (pos? hundreds)
        (doto sb (.append (digit-strings hundreds)) (.append "hundred")))
      (if (and (pos? hundreds) (or (pos? tens) (pos? ones)))
        (.append sb "and"))
      (if (pos? tens)
        (if (= tens 1)
          (.append sb (teens ones))
          (.append sb (decis tens))))
      (if (and (pos? ones) (not= 1 tens))
        (.append sb (digit-strings ones)))
      (.toString sb))))

(->> 1000 inc range (drop 1) (map parse-to-string) (map count) (reduce +))
              
;factorial digit sum
(->> 100 utils/fact utils/num-to-digits (reduce +))

;maximum path sum
(use '[clojure.java.io :as io])

(def path
  (let [file (resource "problem18.txt")]
    (with-open [rdr (io/reader file)]
      (vec (map utils/string-to-numarray (line-seq rdr))))))
      
  
