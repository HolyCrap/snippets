(ns utils)

(defn- enqueue [sieve candidate step]
      (let [m (+ candidate step)]
            (if (sieve m)
                 (recur sieve m step)
                 (assoc sieve m step))))

(defn- next-sieve [sieve candidate]
        (if-let [step (sieve candidate)]
                (-> (dissoc sieve candidate)
                      (enqueue candidate step))
                (enqueue sieve candidate (+ candidate candidate))))

(defn- next-primes [sieve candidate]
         (if (sieve candidate)
              (next-primes (next-sieve sieve candidate) (+ 2 candidate))
              (cons candidate 
                       (lazy-seq (next-primes (next-sieve sieve candidate) (+ 2 candidate))))))

(def primes (concat [2] (next-primes {} 3)))

(defn fact [x]
  (->> x inc range (map bigint) (drop 1) (reduce *)))

(defn num-to-digits [number]
  (let [split #(clojure.string/split % #"")]
     (->> number str split seq (map read-string))))

(defn string-to-numarray [string]
  (let [split #(clojure.string/split % #" ")
        toInt #(new Integer %)]
    (->> string split (map toInt) vec)))