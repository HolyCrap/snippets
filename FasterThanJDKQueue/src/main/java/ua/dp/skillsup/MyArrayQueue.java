package ua.dp.skillsup;

import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;

public class MyArrayQueue<E> extends AbstractCollection<E> implements Queue<E> {
    private volatile AtomicLong head = new AtomicLong(0);
    private volatile Object a;
    private volatile Object aa;
    private volatile Object aaa;
    private volatile Object aaaa;
    private volatile Object aaaaa;
    private volatile Object aaaaaa;
    private volatile Object aaaaaaa;
    private volatile AtomicLong tail = new AtomicLong(0);
    private long falseHead = 0;
    private long falseTail = 0;
    private final E[] array;
    private final int capacity;
    private final int trueCapacity;

    @SuppressWarnings("unchecked")
    public MyArrayQueue(Class<E> clazz, int capacity) {
        this.trueCapacity = capacity;
        this.capacity = Integer.highestOneBit(capacity) * 2;
        array = (E[]) Array.newInstance(clazz, this.capacity + 1);
    }

    @Override
    public boolean offer(E e) {
        if (tail.get() - falseHead > trueCapacity) {
            long currentHead = falseHead = head.get();
            if (tail.get() - currentHead > trueCapacity) {
                return false;
            }
        }
        long currentTail = tail.get();
        array[(int) (currentTail & capacity)] = e;
        tail.lazySet(++currentTail);
        return true;
    }

    @Override
    public E poll() {
        if (head.get() >= falseTail) {
            long currentTail = falseTail = tail.get();
            if (head.get() >= currentTail) {
                return null;
            }
        }
        long currentHead = head.get();
        E val = array[(int) (currentHead & capacity)];
        head.lazySet(++currentHead);
        return val;
    }

    @Override
    public E remove() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public E element() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public E peek() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<E> iterator() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

}
