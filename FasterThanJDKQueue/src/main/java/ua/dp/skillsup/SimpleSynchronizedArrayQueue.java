package ua.dp.skillsup;

import java.lang.reflect.Array;
import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

@SuppressWarnings("unchecked")
public class SimpleSynchronizedArrayQueue<E> extends AbstractQueue<E> {
    private final AtomicReference<E>[] array;
    private final AtomicInteger tail = new AtomicInteger(0);
    private volatile Object a;
    private volatile Object aa;
    private volatile Object aaa;
    private volatile Object aaaa;
    private volatile Object aaaaa;
    private volatile Object aaaaaa;
    private volatile Object aaaaaaa;
    private final AtomicInteger head = new AtomicInteger(0);
    private final int arrayCapacity;
    private final int modulus;

    public SimpleSynchronizedArrayQueue(Class<E> clazz, int capacity) {
        this.arrayCapacity = Integer.highestOneBit(capacity) * 2;
        this.modulus = arrayCapacity - 1;
        this.array = (AtomicReference<E>[]) Array.newInstance(AtomicReference.class, arrayCapacity);
        for (int i = 0; i < arrayCapacity; i++) {
            array[i] = new AtomicReference<>(null);
        }
    }

    @Override
    public boolean offer(E e) {
        int index = tail.getAndIncrement() & modulus;
        return array[index].compareAndSet(null, e);
    }

    @Override
    public E poll() {
        int index = head.getAndIncrement() & modulus;
        return array[index].getAndSet(null);
    }

    @Override
    public E peek() {
        return array[head.get() & modulus].get();
    }

    @Override
    public Iterator<E> iterator() {
        return getElemsStream().filter(e -> e != null).iterator();
    }


    @Override
    public int size() {
        return getElemsStream().mapToInt(e -> e == null ? 0 : 1).sum();
    }

    private Stream<E> getElemsStream() {
        return Arrays.stream(array).map(AtomicReference::get);
    }
}
