package lrulist;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class LRUCache<K, V> implements LRUList<K, V> {
    private final ConcurrentMap<K, AtomicReference<Node>> map = new ConcurrentHashMap<>();
    private final AtomicReference<Node> tail = new AtomicReference<>();
    private final AtomicReference<Node> head = new AtomicReference<>();
    private final ConcurrentLinkedQueue<Node> toRemove = new ConcurrentLinkedQueue<>();
    private final int capacity;
    private final AtomicInteger size = new AtomicInteger(0);
    private final AtomicBoolean purgeInProgress = new AtomicBoolean(false);
    private final Node DUMMY = new Node(null, null);

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public V get(K key) {
        AtomicReference<LRUCache<K, V>.Node> nodeRef = map.get(key);
        if (nodeRef != null) {
            Node node = nodeRef.get();
            Node newNode = new Node(node.key, node.value);
            nodeRef.compareAndSet(node, newNode);
            offer(newNode);
            node.removed = true;
            toRemove.add(node);
            purge();
            return node.value;
        } else {
            return null;
        }
    }

    private void purge() {
        if (purgeInProgress.compareAndSet(false, true)) {
            Node node = null;
            while ((node = toRemove.poll()) != null) {
                unlink(node);
            }
            purgeInProgress.set(false);
        }
    }

    private void unlink(Node node) {
        Node prev = node.prev;
        Node next = node.next;
        if (prev == null && next == null) {
            return;
        }
        if (prev != null) {
            prev.next = next;
        } else {
            head.compareAndSet(node, next);
        }
        if (next != null) {
            next.prev = prev;
        }
        node.prev = null;
        node.next = null;
    }

    private boolean offer(Node node) {
        if (tail.compareAndSet(null, node)) {
            head.lazySet(node);
            return size.incrementAndGet() > capacity;
        }
        if (node == tail.get())
            return false;
        Node prevTail = tail.getAndSet(node);
        prevTail.next = node;
        node.prev = prevTail;
        return size.incrementAndGet() > capacity;
    }

    @Override
    public V put(K key, V value) {
        Node node = new Node(key, value);
        AtomicReference<Node> nodeRef = new AtomicReference<>(node);
        AtomicReference<Node> prev = map.putIfAbsent(key, nodeRef);
        if (prev != null) {
            return prev.get().value;
        } else {
            if (offer(node)) {
                evict();
            }
            purge();
            return value;
        }
    }

    private void evict() {
        while (true) {
            Node node = head.get();
            if (node == null || node.removed)
                continue;
            Node next = node.next;
            if (next != null && head.compareAndSet(node, next)) {
                map.remove(node.key);
                size.decrementAndGet();
                node.removed = true;
                toRemove.add(node);
                break;
            }
        }

    }

    @Override
    public Iterator<lrulist.LRUList.Entry<K, V>> iterator() {
        Node node = tail.get();
        LinkedList<lrulist.LRUList.Entry<K, V>> newList = new LinkedList<>();
        while (node != null) {
            newList.addFirst(node);
            node = node.prev;
        }
        return newList.iterator();
    }

    private class Node implements Entry<K, V> {
        final K key;
        final V value;
        volatile Node next;
        volatile Node prev;
        volatile boolean removed = false;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "Node [key=" + key + ", value=" + value + "]";
        }
    }
}
