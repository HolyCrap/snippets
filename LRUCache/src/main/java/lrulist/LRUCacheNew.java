package lrulist;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class LRUCacheNew<K, V> implements LRUList<K, V> {
    private final ConcurrentMap<K, MyEntry> map = new ConcurrentHashMap<>();
    private final AtomicReference<Node> tail = new AtomicReference<>();
    private final AtomicReference<Node> head = new AtomicReference<>();
    private final ConcurrentLinkedQueue<Node> toRemove = new ConcurrentLinkedQueue<>();
    private int toRemoveSize = 0;
    private final int capacity;
    private final AtomicInteger size = new AtomicInteger(0);
    private final AtomicBoolean purgeInProgress = new AtomicBoolean(false);

    public LRUCacheNew(int capacity) {
        super();
        this.capacity = capacity;
    }

    @Override
    public V get(K key) {
        MyEntry entry = map.get(key);
        if (entry != null) {
            offer(entry);
            purge();
            return entry.value;
        }
        return null;
    }

    private void purge() {
        if (toRemoveSize > capacity && purgeInProgress.compareAndSet(false, true)) {
            Node node = null;
            while ((node = toRemove.poll()) != null) {
                toRemoveSize--;
                Node prev = node.prev;
                Node next = node.next;
                if (prev != null) {
                    prev.next = next;
                } else {
                    head.compareAndSet(node, next);
                }
                if (next != null) {
                    next.prev = prev;
                }
                node.prev = null;
                node.next = null;
            }
            purgeInProgress.set(false);
        }
    }

    @Override
    public V put(K key, V value) {
        MyEntry entry = new MyEntry(key, value);
        MyEntry existing = map.putIfAbsent(key, entry);
        if (existing == null) {
            if (offer(entry)) {
                evict();
            }
            return entry.value;
        }
        return value;
    }

    private void evict() {
        while (true) {
            Node prevHead = head.get();
            if (prevHead == null) {
                continue;
            }
            Node next = prevHead.next;
            if (next != null && head.compareAndSet(prevHead, next)) {
                prevHead.next.prev = null;
                prevHead.next = null;
                MyEntry entry = prevHead.entry;
                if (entry == null) {
                    continue;
                }
                map.remove(entry.key);
                size.decrementAndGet();
                prevHead.entry = null;
                break;
            }
        }
    }

    private boolean offer(MyEntry entry) {
        boolean result = false;
        Node newNode = new Node(entry);
        if (tail.compareAndSet(null, newNode)) {
            head.lazySet(newNode);
            return size.incrementAndGet() > capacity;
        }
        Node tailNode = tail.get();
        if (tailNode.entry != entry) {
            Node currentNode = entry.node.get();
            if (entry.node.compareAndSet(currentNode, newNode)) {
                tailNode = tail.getAndSet(newNode);
                result = size.incrementAndGet() > capacity;
                if (tailNode != null) {
                    tailNode.next = newNode;
                    newNode.prev = tailNode;
                }
                if (currentNode != null) {
                    currentNode.entry = null;
                    toRemove.add(currentNode);
                    toRemoveSize++;
                }
            }
        }
        return result;
    }

    @Override
    public Iterator<lrulist.LRUList.Entry<K, V>> iterator() {
        List<lrulist.LRUList.Entry<K, V>> entries = toList();
        return entries.iterator();
    }

    public List<lrulist.LRUList.Entry<K, V>> toList() {
        List<lrulist.LRUList.Entry<K, V>> entries = new LinkedList<>();
        Node node = head.get();
        while (node != null) {
            MyEntry entry = node.entry;
            if (entry != null) {
                entries.add(entry);
            }
            node = node.next;
        }
        return entries;
    }

    private class Node {
        volatile MyEntry entry;
        volatile Node next;
        volatile Node prev;

        public Node(MyEntry entry) {
            super();
            this.entry = entry;
        }

        @Override
        public String toString() {
            LRUCacheNew<K, V>.MyEntry nextString = next == null ? null : next.entry;
            LRUCacheNew<K, V>.MyEntry prevString = prev == null ? null : prev.entry;
            return "Node [entry=" + entry + ", next=" + nextString + ", prev="
                    + prevString + "]";
        }

    }

    private class MyEntry implements LRUList.Entry<K, V> {
        private final K key;
        private final V value;
        AtomicReference<Node> node = new AtomicReference<>();

        public MyEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "MyEntry [key=" + key + ", value=" + value + "]";
        }
    }
}
