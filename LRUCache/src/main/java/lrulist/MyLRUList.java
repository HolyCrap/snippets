package lrulist;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

public class MyLRUList<K,V> implements LRUList<K, V> {
    private final ConcurrentMap<K, Node> map;
    private final ConcurrentLinkedQueue<Entry<K, V>> queue;
    private final long totalCapacity;
    private final AtomicLong currentSize;

    public MyLRUList(int capacity) {
        map = new ConcurrentHashMap<>();
        queue = new ConcurrentLinkedQueue<>();
        currentSize = new AtomicLong();
        totalCapacity = capacity;
    }

    @Override
    public V get(K key) {
        Node node = map.get(key);
        if (node != null) {
            queue.remove(node);
            queue.offer(node);
            return node.value;
        } else {
            return null;
        }
    }

    @Override
    public V put(K key, V value) {
        Node node = new Node(key, value);
        Node prev = map.putIfAbsent(key, node);
        if (prev != null) {
            return prev.value;
        } else {
            queue.add(node);
            if (currentSize.incrementAndGet() > totalCapacity) {
                Entry<K, V> poll = queue.poll();
                map.remove(poll.getKey());
                currentSize.decrementAndGet();
            }
            return value;
        }
    }

    @Override
    public Iterator<LRUList.Entry<K, V>> iterator() {
        return queue.iterator();
    }

    private class Node implements Entry<K, V> {
        final K key;
        final V value;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }
    }
}
