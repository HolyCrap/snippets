package com.luxoft.dnepr.courses.unit3.controller;

import java.util.*;
import com.luxoft.dnepr.courses.unit3.model.*;

public final class Deck {

	private Deck() {
		
	}
	
	public static List<Card> createDeck(int size) {
		if (size < 1) {
			size = 1;
		}
		if (size > 10) {
			size = 10;
		}
		ArrayList<Card> current = new ArrayList<Card>();
		for (int i = 0; i < size; i++) {
			for (Suit s : Suit.values()) {
				for (Rank r : Rank.values()) {
					current.add(new Card(r,s));
				}
			}
		}
		return current;
	}

	public static int costOf(List<Card> hand) {
		int result = 0;
		for (Card c: hand) {
			if (result >= 21 && c.getRank() == Rank.RANK_ACE) {
				result += c.getCost() - 10;
			}
			else {
				result += c.getCost();
			}	
		}
		return result;
	}
}
