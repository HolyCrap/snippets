package com.luxoft.dnepr.courses.unit3.controller;

import java.util.*;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class GameController {
	private static GameController controller;
	private static List<Card> playerHand;
	private static List<Card> dealerHand;
	private static List<Card> deck;
	private static int deckSize = 3;
	
	private GameController() {
		playerHand = new ArrayList<Card>();
		dealerHand = new ArrayList<Card>();
		deck = Deck.createDeck(deckSize);
	}
	
	public static GameController getInstance() {
		if (controller == null) {
			controller = new GameController();
		}
		
		return controller;
	}
	
	public void newGame() {
		newGame(new Shuffler() {
			
			@Override
			public void shuffle(List<Card> deck) {
				Collections.shuffle(deck);
			}
		});
	}
	
	/**
	 * Создает новую игру.
	 * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
	 * - раздает две карты игроку
	 * - раздает одну карту диллеру.
	 * 
	 * @param shuffler
	 */
	void newGame(Shuffler shuffler) {

		if (deck.size() <= (int)52*deckSize/3) {
			deck = Deck.createDeck(deckSize);
		}
		playerHand.clear();
		dealerHand.clear();
		shuffler.shuffle(deck);
		pickFromDeck(playerHand,2);
		pickFromDeck(dealerHand,1);
	}

	/**
	 * Метод вызывается когда игрок запрашивает новую карту.
	 * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
	 * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
	 * 
	 * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
	 */
	public boolean requestMore() {
		if (Deck.costOf(playerHand) <= 21 && deck.size() != 0) {
			pickFromDeck(playerHand,1);
		}
		if (Deck.costOf(playerHand) <= 21) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
	 * Сдаем диллеру карты пока у диллера не наберется 17 очков.
	 */
	public void requestStop() {
		while(Deck.costOf(dealerHand) < 17) {
			pickFromDeck(dealerHand,1);
		}
	}

	/**
	 * Сравниваем руку диллера и руку игрока. 
	 * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
	 * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
	 * Если очки равны - это пуш (WinState.PUSH)
	 * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
	 */
	public WinState getWinState() {
		int playerCost = Deck.costOf(playerHand);
		int dealerCost = Deck.costOf(dealerHand);
		if (playerCost <= 21) {
			if (dealerCost > 21 || playerCost > dealerCost) {
				return WinState.WIN;
			}
			else if (dealerCost == playerCost) {
				return WinState.PUSH;
			}
			else {
				return WinState.LOOSE;
			}
		}
		else {
			return WinState.LOOSE;
		}
	}

	/**
	 * Возвращаем руку игрока
	 */
	public List<Card> getMyHand() {
		return playerHand;
	}

	/**
	 * Возвращаем руку диллера
	 */
	public List<Card> getDealersHand() {
		return dealerHand;
	}
	
	private void pickFromDeck(List<Card> hand, int times) {
		for (int i = 0; i < times; i++) {
			hand.add(deck.get(0));
			deck.remove(0);
		}
	}
	
	public int getDeckSize() {
		return deck.size();
	}
}
