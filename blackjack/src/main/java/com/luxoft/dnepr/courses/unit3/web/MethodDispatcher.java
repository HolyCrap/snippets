package com.luxoft.dnepr.courses.unit3.web;

import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

public class MethodDispatcher {

	/**
	 * 
	 * @param request
	 * @param response
	 * @return response or <code>null</code> if wasn't able to find a method.
	 */
	public Response dispatch(Request request, Response response) {
		String method = request.getParameters().get("method");
		if (method == null) {
			return null;
		}
		if (method.equals("requestMore")) {
			return requestMore(response);
		}
		if (method.equals("newGame")) {
			return newGame(response);
		}
		if (method.equals("enough")) {
			return requestStop(response);
		}
		return null;
	}

	private Response requestStop(Response response) {
		GameController.getInstance().requestStop();
		response.write("{\"dealerHand\":");
		writeHand(response, GameController.getInstance().getDealersHand());
		response.write(", \"gameState\":\"");
		response.write(GameController.getInstance().getWinState().toString());
		response.write("\"}");
		return response;
	}

	private Response newGame(Response response) {
		GameController.getInstance().newGame();
		response.write("{\"dealerHand\":");
		writeHand(response, GameController.getInstance().getDealersHand());
		response.write(", \"myHand\":");
		writeHand(response, GameController.getInstance().getMyHand());
		response.write(", \"deckSize\":" + GameController.getInstance().getDeckSize());
		response.write("}");
		return response;
	}

	private Response requestMore(Response response) {
		response.write("{\"result\": " + GameController.getInstance().requestMore());
		response.write(", \"myHand\": ");
		
		writeHand(response, GameController.getInstance().getMyHand());
		
		response.write("}");
		
		return response;
	}

	private void writeHand(Response response, List<Card> hand) {
		boolean isFirst = true;
		response.write("[");
		for (Card card : hand) {
			if (isFirst) {
				isFirst = false;
			} else {
				response.write(",");
			}
			response.write("{\"rank\": \"");
			response.write(card.getRank().getStringRankName());
			response.write("\", \"suit\": \"");
			response.write(card.getSuit().name());
			response.write("\", \"cost\": ");
			response.write(String.valueOf(card.getCost()));
			response.write("}");
		}
		response.write("]");
	}

}
