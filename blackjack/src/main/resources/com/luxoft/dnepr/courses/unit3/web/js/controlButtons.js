"use strict";
$(document).ready(function(){
	var clear = $("#cls");
	var more = $("#more");
	var enough = $("#enough");
	var next = $("#next");
	var dealer = $("#dealer"), player = $("#player"), winState = $("#winState");
	$.ajaxSetup({
		cache: false
	});
	
	clear.click(function(){
		$("#dealerScore").text("0");
		$("#playerScore").text("0");
	});
	
	next.click(function(){
		ajaxMethod("newGame", function(data) {
				dealer.empty();
				player.empty();
				winState.empty();
				loadCard(data.dealerHand[0].rank, data.dealerHand[0].suit,dealer);
				loadCard(data.myHand[0].rank, data.myHand[0].suit,player);
				loadCard(data.myHand[1].rank, data.myHand[1].suit,player);
			});
		NewGameState(false);
	});
	
	more.click(function(){
		ajaxMethod("requestMore", function(data) {
			var lastCardI = data.myHand.length-1;
			loadCard(data.myHand[lastCardI].rank, data.myHand[lastCardI].suit, player);
			if (!data.result) {
				winState.append("<span>You lose</span>");
				NewGameState(true);
				$("#dealerScore").text(parseInt($("#dealerScore").text()) + 1);
			}
		});	
	});
	
	enough.click(function(){
		ajaxMethod("enough", function(data) {
			dealer.empty();
			for (var i = 0; i < data.dealerHand.length; i++) {
				loadCard(data.dealerHand[i].rank, data.dealerHand[i].suit, dealer);
			}
			switch(data.gameState) {
			case 'WIN': 
				winState.append("<span>You win</span>");
				$("#playerScore").text(parseInt($("#playerScore").text()) + 1);
				break;
			case 'LOOSE':
				winState.append("<span>You lose</span>");
				$("#dealerScore").text(parseInt($("#dealerScore").text()) + 1);
				break;
			case 'PUSH':
				winState.append("<span>It's a draw (push)</span>");
				break;
			default:
				alert("Wrong game state");
			}
			NewGameState(true);
		});
	});
	
	function loadCard(rank, suit, where) {
		var img = $("<img />").load(function(){
			where.append(img);
		}).attr('src', '../images/cards/'+rank+'_'+suit+'.png'+"?" + new Date().getTime());
	};
	
	function ajaxMethod(method, onSuccess) {
		$.ajax({
			url:'/service',
			type:"GET",
			data:"method="+method,
			dataType:"json",
			success: onSuccess,
			error: function() {
				alert("Error in json format");
			}
		});
	}
	
	function NewGameState(val) {
		if (val) {
			$("#next").attr("disabled",false);
			$("#more").attr("disabled",true);
			$("#enough").attr("disabled",true);
		}
		else {
			$("#next").attr("disabled",true);
			$("#more").attr("disabled",false);
			$("#enough").attr("disabled",false);
		}		
	}	
});