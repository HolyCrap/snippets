"use strict";
$(document).ready(function(){
	var hb = $("#helpButton");
	var help = $("#help");
	hb.click(function(){
		if(help.css("display") === "none") {			
			help.show();
		}
		else {
			help.hide();
		}
	});
});