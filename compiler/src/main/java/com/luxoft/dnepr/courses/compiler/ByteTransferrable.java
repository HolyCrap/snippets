package com.luxoft.dnepr.courses.compiler;

public interface ByteTransferrable {
	public byte[] convertToBytes();
}
