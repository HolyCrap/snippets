package com.luxoft.dnepr.courses.compiler;

public enum Command implements ByteTransferrable{
	ADD((byte)0x05),
	MULTIPLY((byte)0x06),
	SUBTRACT((byte)0x07),
	DIVIDE((byte)0x08),
	PUSH((byte)0x01);
	
	Command (byte byteCode) {
		this.byteCode = byteCode;
	}
	private byte byteCode;
	public byte getByteCode() {
		return byteCode;
	}
	@Override
	public byte[] convertToBytes() {
		return new byte[]{byteCode};
	}
}

