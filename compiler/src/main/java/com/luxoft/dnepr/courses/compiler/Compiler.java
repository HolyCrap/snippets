package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {

	public static final boolean DEBUG = true;

	public static void main(String[] args) {
		byte[] byteCode = compile(getInputString(args));
		VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
		vm.run();
	}

	static byte[] compile(String input) {
		if (input == null) {
			throw new CompilationException("Input string is null");
		}
		input = input.replaceAll("[\\s]", "");
		input = CompilerUtils.appendWithZeros(input);
		if (!input.matches("[0-9\\Q.+-*/()\\E]+")) {
			throw new CompilationException("Wrong symbols found. "
					+ "Float point numbers, *,/,+ or - expected");
		}
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		input = input.replaceAll("[\\s]", "");
		Operation last = CompilerUtils.execute(input);	
		writeBytes(result,last.convertToBytes());
		addCommand(result,(byte) 0x09);
		return result.toByteArray();
	}

	/**
	 * Adds specific command to the byte stream.
	 * 
	 * @param result
	 * @param command
	 */
	public static void addCommand(ByteArrayOutputStream result, byte command) {
		result.write(command);
	}

	/**
	 * Adds specific command with double parameter to the byte stream.
	 * 
	 * @param result
	 * @param command
	 * @param value
	 */
	public static void addCommand(ByteArrayOutputStream result, byte command,
			double value) {
		result.write(command);
		writeDouble(result, value);
	}

	private static void writeDouble(ByteArrayOutputStream result, double val) {
		long bits = Double.doubleToLongBits(val);

		result.write((byte) (bits >>> 56));
		result.write((byte) (bits >>> 48));
		result.write((byte) (bits >>> 40));
		result.write((byte) (bits >>> 32));
		result.write((byte) (bits >>> 24));
		result.write((byte) (bits >>> 16));
		result.write((byte) (bits >>> 8));
		result.write((byte) (bits >>> 0));
	}

	private static String getInputString(String[] args) {
		if (args.length > 0) {
			return join(Arrays.asList(args));
		}

		Scanner scanner = new Scanner(System.in);
		List<String> data = new ArrayList<String>();
		while (scanner.hasNext()) {
			data.add(scanner.next());
		}
		scanner.close();
		return join(data);
	}

	private static String join(List<String> list) {
		StringBuilder result = new StringBuilder();
		for (String element : list) {
			result.append(element);
		}
		return result.toString();
	}
	
	public static void writeBytes(ByteArrayOutputStream result, byte[] input) {
		for (byte b: input) {
			result.write(b);
		}
	}
}
