package com.luxoft.dnepr.courses.compiler;

import java.util.ArrayList;

public final class CompilerUtils {

	private CompilerUtils() {

	}

	public static ArrayList<ByteTransferrable> formulaToArrayList(String str) {
		char[] charred = str.toCharArray();
		ArrayList<ByteTransferrable> list = new ArrayList<ByteTransferrable>();
		for (int i = 0; i < charred.length; i++) {
			switch (charred[i]) {
			case '(': 
				int temp = -1;
				int openedCount = 0;
				for (int j = i; j < charred.length; j++) {
					if (charred[j] == '(')
						openedCount++;
					if (charred[j] == ')')
						openedCount--;
					if (charred[j] == ')' && openedCount == 0) {
						temp = j;
						break;
					}
				}
				if (temp == -1 || openedCount !=0)
					throw new CompilationException("Wrong brackets");
				else {
					String recursive = str.substring(i + 1, temp);
					list.add(execute(recursive));
					i = temp;
					if (i >= charred.length)
						break;
				}
				break;
			case '/':
				list.add(Command.DIVIDE);
				break;
			case '*':
				list.add(Command.MULTIPLY);
				break;
			case '-':
				list.add(Command.SUBTRACT);
				break;
			case '+':
				list.add(Command.ADD);
				break;
			case ')':
				throw new CompilationException("Wrong brackets");
			default:
				String number = "";
				int k = i;
				while (charred[k] != '*' && charred[k] != '('
						&& charred[k] != '/' && charred[k] != '-'
						&& charred[k] != '+' && charred[k] != ')') {
					number = number + charred[k];
					k++;
					if (k >= charred.length)
						break;
				}
				i = k - 1;
				list.add(new NumberContainer(Double.parseDouble(number)));
			}
		}
		return list;
	}

	public static Operation execute(String str) {
		ArrayList<ByteTransferrable> list = formulaToArrayList(str);
		while (list.size() != 1) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i) == Command.MULTIPLY
						|| list.get(i) == Command.DIVIDE) {
					elementsToOperation(list, i);
				}
			}
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i) == Command.ADD
						|| list.get(i) == Command.SUBTRACT) {
					elementsToOperation(list, i);
				}
			}
		}
		return (Operation) list.get(0);

	}

	private static void elementsToOperation(ArrayList<ByteTransferrable> list,
			int i) {
		ByteTransferrable value1 = list.get(i - 1);
		ByteTransferrable value2 = list.get(i + 1);
		Command op = (Command) list.get(i);
		Operation temp = new Operation(value1.convertToBytes(), op,
				value2.convertToBytes());
		list.remove(i - 1);
		list.remove(i - 1);
		list.remove(i - 1);
		list.add(i - 1, temp);
	}
	
	public static String appendWithZeros(String input) {
		for (int i = 0; i < input.length(); i++) {
			if (i == 0 && input.charAt(0) == '-') {
				input = '0' + input;
			}
			if (input.charAt(i) == '(' && input.charAt(i+1)=='-') {
				input = input.substring(0,i) + "(0" + input.substring(i+1,input.length());
			}
		}
		return input;
	}
}
