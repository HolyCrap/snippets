package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;

public class NumberContainer implements ByteTransferrable {
	@Override
	public String toString() {
		return ""+value;
	}

	double value;
	
	public NumberContainer (double value) {
		this.value = value;
	}

	@Override
	public byte[] convertToBytes() {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		Compiler.addCommand(result, Command.PUSH.getByteCode(), value);
		return result.toByteArray();
	}
	
}
