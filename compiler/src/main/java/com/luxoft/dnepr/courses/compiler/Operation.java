package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;



public class Operation implements ByteTransferrable{
	@Override
	public String toString() {
		String s = "";
		for (byte b: opResult) {
			s += b;
		}
		return s;
	}

	private byte[] value1;
	private byte[] value2;
	private Command operation;
	private byte[] opResult;
	
	public Operation (byte[] number1, Command op, byte[] number2) {
		value1 = number1;
		value2 = number2;
		operation = op;
		opResult = operate();
	}
	
	private byte[] operate() {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		switch (operation) {
		case ADD:
			addValues(result, value1, value2);
			Compiler.addCommand(result, Command.ADD.getByteCode());
			break;
		case DIVIDE:
			addValues(result, value2, value1);
			Compiler.addCommand(result, Command.DIVIDE.getByteCode());
			break;
		case SUBTRACT:
			addValues(result, value2, value1);
			Compiler.addCommand(result, Command.SUBTRACT.getByteCode());
			break;
		case MULTIPLY:
			addValues(result, value1, value2);
			Compiler.addCommand(result, Command.MULTIPLY.getByteCode());
			break;
		default: break;
		}
		opResult = result.toByteArray();
		return opResult;
	}
	
	private static void addValues(ByteArrayOutputStream result, byte[] v1, byte[] v2) {
		Compiler.writeBytes(result, v1);
		Compiler.writeBytes(result, v2);
	}

	@Override
	public byte[] convertToBytes() {
		return operate();
	}
	
	
}
