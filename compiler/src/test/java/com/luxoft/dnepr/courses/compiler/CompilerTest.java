package com.luxoft.dnepr.courses.compiler;

import static org.junit.Assert.*;
import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

	@Test
	public void testSimple() {
		assertCompiled(4, "2+2");
		assertCompiled(5, " 2 + 3 ");
		assertCompiled(1, "2 - 1 ");
		assertCompiled(6, " 2 * 3 ");
		assertCompiled(4.5, "  9 /2 ");
		assertCompiled(4,"2.0+2.0");
	}
	
	@Test
	public void testComplex() {
		assertCompiled(12, "  (2 + 2 ) * 3 ");
		assertCompiled(8.5, "  2.5 + 2 * 3 ");
		assertCompiled(8.5, "  2 *3 + 2.5");
		assertCompiled(0, "5*(3-4*3)+10*(7-5/2)");
		assertCompiled(-932,"3+((7-4*(21+4))*10-5)");
		assertCompiled(12, "  (2.0 + 2 ) * 3 ");
		assertCompiled(9,"3*6/2");
		assertCompiled(9,"3/2*6");
		assertCompiled(-18,"-3+(2-((-3)+5*4))");
		//assertCompiled(48,"4*8/9*16");
	}
	
	@Test(expected = CompilationException.class)
	public void wrongSyntaxTest() {
		assertCompiled(12, "  @(2 + 2 ) * 3 ");
	}
	
	@Test(expected = CompilationException.class)
	public void wrongSyntaxTest2() {
		assertCompiled(12, null);
	}
	@Test(expected = CompilationException.class)
	public void wrongBracketsTest() {
		assertCompiled(12, "  (2 + 2 )) * 3 ");
	}
	
	@Test(expected = CompilationException.class)
	public void wrongBracketsTest2() {
		assertCompiled(12, "  ((2 + 2 ) * 3 ");
	}
	
	@Test
	public void appendWithZerosTest() {
		assertEquals("0-3+(2-((0-3)+5*4))", 
				CompilerUtils.appendWithZeros("-3+(2-((-3)+5*4))"));
		assertEquals("(0-5+4)*(2+(0-4)*2)", 
				CompilerUtils.appendWithZeros("(-5+4)*(2+(-4)*2)"));
	}
}
