package edu.objectpool;

public interface ObjectFactory {
	Object create();
}
