package edu.objectpool;

import java.lang.reflect.Array;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

@SuppressWarnings("unchecked")
public class ObjectPool {
	
	private final int CLASTERS_COUNT;
	private final ConcurrentLinkedQueue<PoolEntry>[] pool;
	private final ThreadLocal<Integer> clasterKey;
	private final ThreadLocal<AtomicInteger> clasterSize;
	
	public ObjectPool() {
		CLASTERS_COUNT = Runtime.getRuntime().availableProcessors() * 4;
		clasterKey = initThreadLocal(() -> Integer.valueOf(0));
		clasterSize = initThreadLocal(AtomicInteger::new);
		pool = (ConcurrentLinkedQueue<PoolEntry>[]) Array.newInstance(
				ConcurrentLinkedQueue.class, CLASTERS_COUNT);
		for (int i = 0; i < pool.length; i++) {
			pool[i] = new ConcurrentLinkedQueue<PoolEntry>();
		}
	}

	public PoolEntry pull() {
		PoolEntry entry = pool[clasterKey.get()].poll();
		return entry;
	}

	public void push(PoolEntry entry) {
		pool[clasterKey.get()].offer(entry);
	}
	
	private <E> ThreadLocal<E> initThreadLocal(Supplier<E> initiator) {
		return new ThreadLocal<E>() {

			@Override
			protected E initialValue() {
				return initiator.get();
			}
			
		};
	}
}
