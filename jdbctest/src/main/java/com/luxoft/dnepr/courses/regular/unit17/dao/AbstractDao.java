package com.luxoft.dnepr.courses.regular.unit17.dao;

import java.sql.*;

import com.luxoft.dnepr.courses.regular.unit17.model.Entity;
import com.luxoft.dnepr.courses.regular.unit17.exception.*;

public class AbstractDao<E extends Entity> implements IDao<E> {
	private Class<E> thisClass;
	private ConnectionInfo info;

	/**
	 * @param info
	 *            - contains address, username and password
	 * @param thisClass
	 *            - instance of class of a generic type of DAO
	 */
	public AbstractDao(ConnectionInfo info, Class<E> thisClass) {
		this.thisClass = thisClass;
		if (info == null) {
			throw new IllegalArgumentException("Connection query is null");
		}
		this.info = info;
	}

	@Override
	public E save(E e) throws UserAlreadyExist {
		if (e == null) {
			throw new IllegalArgumentException("Trying to save null object");
		}
		try (Connection conn = this.getConnection()) {
			if (e.getId() == null) {
				e.setId(this.getMaxKeyPlusOne(conn));
			}
			if (this.idAlreadyExists(e.getId(), conn)) {
				throw new UserAlreadyExist(e.getId());
			}
			Statement stmnt = conn.createStatement();
			stmnt.executeUpdate(e.getInsertQuery());
		} catch (SQLException ex) {
			this.handleSQLException(ex);
		}
		return e;
	}

	@Override
	public E update(E e) throws UserNotFound {
		if (e == null) {
			throw new IllegalArgumentException("Trying to update null object");
		}
		try (Connection conn = this.getConnection()) {
			Statement statement = conn.createStatement();
			int rows = statement.executeUpdate(e.getUpdateQuery());
			if (rows == 0) {
				throw new UserNotFound(e.getId());
			}
		} catch (SQLException ex) {
			this.handleSQLException(ex);
		}
		return e;
	}

	@Override
	public E get(int id) {
		E object = null;
		try {
			object = thisClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		try (Connection conn = this.getConnection()) {
			Statement statement = conn.createStatement();
			ResultSet result = statement.executeQuery(QueryConstructor
					.getInstance().constructSelectByIdQuery(
							thisClass.getSimpleName(), id));
			object.readValuesFromDB(result);
		} catch (SQLException ex) {
			this.handleSQLException(ex);
		}
		return object.getId() == null ? null : object;
	}

	@Override
	public boolean delete(int id) {
		try (Connection conn = this.getConnection()) {
			Statement prep = conn.createStatement();
			int rows = prep.executeUpdate(QueryConstructor.getInstance()
					.constructDeleteByIdQuery(thisClass.getSimpleName(), id));
			if (rows > 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException ex) {
			this.handleSQLException(ex);
		}
		return false;
	}

	private Integer getMaxKeyPlusOne(Connection conn) {
		Integer maxId = null;
		try {
			Statement prep = conn.createStatement();
			ResultSet result = prep.executeQuery(QueryConstructor.getInstance()
					.constructMaxIdQuery(thisClass.getSimpleName()));
			while (result.next()) {
				maxId = result.getInt("MAX");
			}
			if (maxId == null) {
				maxId = Integer.valueOf(1);
			}
		} catch (SQLException e) {
			this.handleSQLException(e);
		}
		return Integer.valueOf(maxId.intValue() + 1);
	}

	private boolean idAlreadyExists(Integer id, Connection conn) {
		try {
			Statement statement = conn.createStatement();
			return statement.executeQuery(
					QueryConstructor.getInstance().constructSelectByIdQuery(
							thisClass.getSimpleName(), id)).isBeforeFirst();
		} catch (SQLException e) {
			this.handleSQLException(e);
		}
		return false;
	}

	private Connection getConnection() {
		try {
			return DriverManager.getConnection(info.getAddress(),
					info.getUsername(), info.getPassword());
		} catch (SQLException ex) {
			this.handleSQLException(ex);
		}
		return null;
	}

	private void handleSQLException(SQLException ex) {
		System.out.println("SQLException: " + ex.getMessage());
		System.out.println("SQLState: " + ex.getSQLState());
		System.out.println("VendorError: " + ex.getErrorCode());
	}
}
