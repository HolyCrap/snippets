package com.luxoft.dnepr.courses.regular.unit17.dao;

public class ConnectionInfo {
	private String address;
	private String username;
	private String password;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String query) {
		this.address = query;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
