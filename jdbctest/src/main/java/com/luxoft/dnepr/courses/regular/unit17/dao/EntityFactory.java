package com.luxoft.dnepr.courses.regular.unit17.dao;

import com.luxoft.dnepr.courses.regular.unit17.model.*;

public class EntityFactory {
	public static Redis createRedis(Integer id, int weight) {
		Redis result = new Redis();
		result.setId(id);
		result.setWeight(weight);
		return result;
	}
	
	public static Employee createEmployee(Integer id, int salary) {
		Employee result = new Employee();
		result.setId(id);
		result.setSalary(salary);
		return result;
	}
}
