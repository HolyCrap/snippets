package com.luxoft.dnepr.courses.regular.unit17.dao;

public class QueryConstructor {
	private static QueryConstructor constructor = new QueryConstructor();

	private QueryConstructor() {

	}

	public static QueryConstructor getInstance() {
		return constructor;
	}

	public String constructMaxIdQuery(String tablename) {
		return "SELECT MAX(id) AS MAX FROM " + tablename;
	}
	
	public String constructSelectByIdQuery(String tablename, int id) {
		 return "SELECT * FROM " + tablename + " WHERE ID = " + id;
	}
	
	public String constructDeleteByIdQuery(String tablename, int id) {
		return "DELETE FROM " + tablename + " WHERE id = " + id;
	}
}
