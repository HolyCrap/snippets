package com.luxoft.dnepr.courses.regular.unit17.exception;

@SuppressWarnings("serial")
public class UserAlreadyExist extends RuntimeException {
	private Integer existId;
	public UserAlreadyExist(Integer id) {
		super();
		existId = id;
	}
	public Integer getExistId() {
		return existId;
	}
}
