package com.luxoft.dnepr.courses.regular.unit17.exception;

@SuppressWarnings("serial")
public class UserNotFound extends RuntimeException {
	public Integer notFoundId;

	public UserNotFound(Integer notFoundId) {
		super();
		this.notFoundId = notFoundId;
	}

	public Integer getNotFoundId() {
		return notFoundId;
	}
}
