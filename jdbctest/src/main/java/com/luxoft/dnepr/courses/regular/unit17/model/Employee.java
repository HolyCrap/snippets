package com.luxoft.dnepr.courses.regular.unit17.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Employee extends Entity {
    private int salary;
    public int getSalary() { return salary; }
    public void setSalary(int salary) { this.salary = salary; }
	@Override
	
	public String getInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO Employee (id, salary) VALUES (");
		sb.append(this.getId().intValue());
		sb.append(", ");
		sb.append(this.getSalary());
		sb.append(");");
		return sb.toString();
	}

	/* 
	 * Example: UPDATE Employee SET salary = 2500 WHERE id = 25
	 */
	@Override
	public String getUpdateQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE Employee SET salary = ");
		sb.append(this.getSalary());
		sb.append(" WHERE id = ");
		sb.append(this.getId().intValue());
		return sb.toString();
	}
	@Override
	public void readValuesFromDB(ResultSet rs) throws SQLException {
		while (rs.next()) {
			this.setId(rs.getInt("id"));
			this.setSalary(rs.getInt("salary"));
		}
	}
}