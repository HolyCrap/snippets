package com.luxoft.dnepr.courses.regular.unit17.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Entity {
    private Integer id;
    public Integer getId() { return id; }
    public void setId(Integer id) { this.id = id; }
    public abstract String getInsertQuery();
    public abstract String getUpdateQuery();
    public abstract void readValuesFromDB(ResultSet rs) throws SQLException;
}
