package com.luxoft.dnepr.courses.regular.unit17.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Redis extends Entity {
    private int weight;
    public int getWeight() { return weight; }
    public void setWeight(int weight) { this.weight = weight; }
	@Override
	public String getInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO Redis (id, weight) VALUES (");
		sb.append(this.getId().intValue());
		sb.append(", ");
		sb.append(this.getWeight());
		sb.append(");");
		return sb.toString();
	}
	
	/* 
	 * Example: UPDATE Redis SET weight = 2500 WHERE id = 25
	 */
	@Override
	public String getUpdateQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE Redis SET weight = ");
		sb.append(this.getWeight());
		sb.append(" WHERE id = ");
		sb.append(this.getId().intValue());
		return sb.toString();
	}
	
	@Override
	public void readValuesFromDB(ResultSet rs) throws SQLException {
		while (rs.next()) {
			this.setId(rs.getInt("id"));
			this.setWeight(rs.getInt("weight"));
		}
	}
}
