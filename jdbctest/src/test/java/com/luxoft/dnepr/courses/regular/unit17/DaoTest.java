package com.luxoft.dnepr.courses.regular.unit17;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.luxoft.dnepr.courses.regular.unit17.dao.*;
import com.luxoft.dnepr.courses.regular.unit17.model.*;
import com.luxoft.dnepr.courses.regular.unit17.exception.*;

public class DaoTest {
	public static String startConnection = "jdbc:h2:~/test";
	public static String driverName = "org.h2.Driver";
	public static String username = "sa";
	public static String password = "";
	public static AbstractDao<Employee> employeeDao;
	public static AbstractDao<Redis> redisDao;
	public static ConnectionInfo info;

	@BeforeClass
	public static void init() {
		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		info = new ConnectionInfo();
		info.setAddress(startConnection);
		info.setUsername(username);
		info.setPassword(password);
		
		employeeDao = new AbstractDao<>(info, Employee.class);
		redisDao = new AbstractDao<>(info, Redis.class);

		try (Connection connection = DriverManager.getConnection(
				info.getAddress(), info.getUsername(), info.getPassword())) {	
			connection.setAutoCommit(false);
			Statement createDB = connection.createStatement();
			createDB.addBatch("CREATE SCHEMA IF NOT EXISTS unit17");
			createDB.addBatch("CREATE TABLE IF NOT EXISTS Employee (id INTEGER, salary INTEGER);");
			createDB.addBatch("CREATE TABLE IF NOT EXISTS Redis (id INTEGER, weight INTEGER);");
			createDB.addBatch("TRUNCATE TABLE Employee;");
			createDB.addBatch("TRUNCATE TABLE Redis;");
			createDB.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void employeeTest() {
		employeeDao.save(EntityFactory.createEmployee(Integer.valueOf(12), 3500));
		employeeDao.save(EntityFactory.createEmployee(Integer.valueOf(15), 2000));
		boolean catched = false;
		try {
			employeeDao.save(EntityFactory.createEmployee(Integer.valueOf(12), 1000));
		} catch (UserAlreadyExist ex) {
			catched = true;
		}
		assertTrue(catched);
		checkAdded();
		employeeDao.update(EntityFactory.createEmployee(Integer.valueOf(12), 10000));
		checkUpdated();
		catched = false;
		try {
			employeeDao.update(EntityFactory.createEmployee(Integer.valueOf(11), 100));
		} catch (UserNotFound e) {
			catched = true;
		}
		assertTrue(catched);
		assertTrue(employeeDao.delete(12));
		assertFalse(employeeDao.delete(5));
		checkDeleted();
		Employee got = employeeDao.get(15);
		assertEquals(got.getId(), Integer.valueOf(15));
		assertEquals(got.getSalary(), 2000);
		employeeDao.save(EntityFactory.createEmployee(null, 25000));
		if (employeeDao.get(16) == null) {
			assertTrue(false);
		}
		assertNull(employeeDao.get(1000));
		assertEquals(employeeDao.get(16).getSalary(), 25000);
	}
	
	@Test
	public void redisTest() {
		redisDao.save(EntityFactory.createRedis(Integer.valueOf(1), 15));
		redisDao.save(EntityFactory.createRedis(Integer.valueOf(2), 20));
		assertNotNull(redisDao.get(1));
		assertNotNull(redisDao.get(2));
		assertEquals(redisDao.get(1).getWeight(), 15);
		redisDao.update(EntityFactory.createRedis(Integer.valueOf(1), 25));
		assertEquals(redisDao.get(1).getWeight(), 25);
	}
	
	private void checkDeleted() {
		try (Connection connection = DriverManager.getConnection(
				info.getAddress(), info.getUsername(), info.getPassword())) {
			ResultSet result = connection.createStatement().executeQuery(
					"SELECT * FROM Employee WHERE id = 12");
			assertFalse(result.isBeforeFirst());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void checkAdded() {
		try (Connection connection = DriverManager.getConnection(
				info.getAddress(), info.getUsername(), info.getPassword())) {
			ResultSet result = connection.createStatement().executeQuery(
					"SELECT * FROM Employee ORDER BY id");
			if (result.next()) {
				assertEquals(result.getInt("id"), 12);
				assertEquals(result.getInt("salary"), 3500);
			} else {
				assertTrue(false);
			}
			if (result.next()) {
				assertEquals(result.getInt("id"), 15);
				assertEquals(result.getInt("salary"), 2000);
			} else {
				assertTrue(false);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void checkUpdated() {
		try (Connection connection = DriverManager.getConnection(
				info.getAddress(), info.getUsername(), info.getPassword())) {
			ResultSet result = connection.createStatement().executeQuery(
					"SELECT salary FROM Employee WHERE id = 12;");
			if (result.next()) {
				assertEquals(result.getInt("salary"), 10000);
			} else {
				assertTrue(false);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public static void destroy() {
		try (Connection connection = DriverManager.getConnection(
				info.getAddress(), info.getUsername(), info.getPassword())) {
			connection.setAutoCommit(false);
			Statement query = connection.createStatement();
			query.addBatch("TRUNCATE TABLE Employee;");
			query.addBatch("TRUNCATE TABLE Redis;");
			query.addBatch("DROP TABLE Employee;");
			query.addBatch("DROP TABLE Redis;");
			query.addBatch("DROP SCHEMA IF EXISTS unit17");
			query.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
