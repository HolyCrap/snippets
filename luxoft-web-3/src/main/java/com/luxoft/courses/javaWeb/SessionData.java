package com.luxoft.courses.javaWeb;

import java.util.concurrent.atomic.AtomicLong;

public class SessionData {
	public static final AtomicLong activeSessions = new AtomicLong(0);
	public static final AtomicLong activeAdminSessions = new AtomicLong(0);
	public static final AtomicLong activeUserSessions = new AtomicLong(0);
	public static final AtomicLong requestsCount = new AtomicLong(0);
	public static final AtomicLong POSTCount = new AtomicLong(0);
	public static final AtomicLong GETCount = new AtomicLong(0);
	public static final AtomicLong OtherCount = new AtomicLong(0);
}
