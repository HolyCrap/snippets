package com.luxoft.courses.javaWeb.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.luxoft.courses.javaWeb.ServletUtils;

public class SessionDataFilter implements Filter {
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("username") == null) {
			response.sendRedirect(request.getContextPath() + ServletUtils.INDEX);
			return;
		}
		String role = (String) session.getAttribute("role");
		switch (role) {
		case "user":
			response.sendRedirect(request.getContextPath() + "/user");
			break;
		case "admin":
			request.getRequestDispatcher("/WEB-INF/sessionData.jsp").forward(request, response);
			break;
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
