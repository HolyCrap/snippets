package com.luxoft.courses.javaWeb.listeners;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

import com.luxoft.courses.javaWeb.SessionData;

public class RequestCounter implements ServletRequestListener {
	@Override
	public void requestInitialized(ServletRequestEvent event) {
		SessionData.requestsCount.incrementAndGet();
		HttpServletRequest request = (HttpServletRequest) event.getServletRequest();
		if (request.getMethod().equals("GET"))
			SessionData.GETCount.incrementAndGet();
		else if (request.getMethod().equals("POST"))
			SessionData.POSTCount.incrementAndGet();
		else SessionData.OtherCount.incrementAndGet();
	}

	@Override
	public void requestDestroyed(ServletRequestEvent event) {
	}


}
