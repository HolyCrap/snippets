package com.luxoft.courses.javaWeb.listeners;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import com.luxoft.courses.javaWeb.SessionData;

public class RoleListener implements HttpSessionAttributeListener{

	@Override
	public void attributeAdded(HttpSessionBindingEvent event) {
		if (event.getName().equals("role")) {
			String attr = (String) event.getValue();
			switch (attr) {
			case "admin": SessionData.activeAdminSessions.incrementAndGet(); break;
			case "user": SessionData.activeUserSessions.incrementAndGet(); break;
			}
		}
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent event) {
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent event) {
	}

}
