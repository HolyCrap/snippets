package com.luxoft.courses.javaWeb.listeners;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.luxoft.courses.javaWeb.SessionData;

public class SessionCounter implements HttpSessionListener{

	@Override
	public void sessionCreated(HttpSessionEvent event) {	
		SessionData.activeSessions.incrementAndGet();
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		SessionData.activeSessions.decrementAndGet();
		HttpSession session = event.getSession();
		String role = (String) session.getAttribute("role");
		if (role == null) return;
		switch (role) {
		case "user":
			SessionData.activeUserSessions.decrementAndGet();
			break;
		case "admin":
			SessionData.activeAdminSessions.decrementAndGet();
			break;		
		}
	}
	
}
