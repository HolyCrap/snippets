<%@page import="com.luxoft.courses.javaWeb.SessionData"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Session data</title>
</head>
<body style="{width:500px; text-align:center;}">
	<table border="1">
		<tr>
			<td>Parameter</td>
			<td>Value</td>
		</tr>
		<tr>
			<td>Active sessions</td>
			<td><%=SessionData.activeSessions %></td>
		</tr>
		<tr>
			<td>Active admin sessions</td>
			<td> <%=SessionData.activeAdminSessions %></td>
		</tr>
		<tr>
			<td>Active user sessions</td>
			<td><%=SessionData.activeUserSessions %></td>
		</tr>
		<tr>
			<td>Total count of HttpRequests</td>
			<td><%=SessionData.requestsCount %></td>
		</tr>
		<tr>
			<td>Total count of POST requests</td>
			<td><%=SessionData.POSTCount %></td>
		</tr>
		<tr>
			<td>Total count of GET requests</td>
			<td><%=SessionData.GETCount %></td>
		</tr>
		<tr>
			<td>Total count of Other requests</td>
			<td><%=SessionData.OtherCount%></td>
		</tr>
	</table>
</body>
</html>