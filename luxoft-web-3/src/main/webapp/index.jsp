<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>Main page</title>
<meta content="no-cache" />
<link rel="stylesheet" href="layout.css" type="text/css" />
</head>
<body>
	<div class="centered">
		<div id="names">
			<div class="field">
				<p>Name:</p>
			</div>
			<div class="field">
				<p>Password</p>
			</div>
		</div>
		<div id="values">
			<form action="../myFirstWebApp/user" method="post">
				<div>
					<input type="text" name="name" />
				</div>
				<div>
					<input type="password" name="password" />
				</div>
				<div>
					<input type="submit" name="submit" value="Sign in" />
				</div>
			</form>
		</div>
		<div id="errorMessage">
			${message}
		</div>
	</div>
</body>
</html>