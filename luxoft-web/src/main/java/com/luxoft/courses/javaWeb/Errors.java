package com.luxoft.courses.javaWeb;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

public final class Errors {
	private Errors() {
	}
	
	public static void internalServerError(HttpServletResponse response,
			String message) throws IOException {
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		response.setContentType("application/json; charset=utf-8");
		response.setContentLength(message.length());
		response.getWriter().write(message);
	}
}
