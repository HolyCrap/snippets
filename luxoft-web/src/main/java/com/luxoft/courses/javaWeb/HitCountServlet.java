package com.luxoft.courses.javaWeb;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HitCountServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3295958738201714545L;
	private static AtomicInteger counter = new AtomicInteger(0);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setStatus(200);
		resp.setContentType("application/json; charset=utf-8");
		StringBuilder sb = new StringBuilder();
		sb.append("{\"hitCount\":");
		sb.append(counter.incrementAndGet());
		sb.append("}");
		resp.setContentLength(sb.toString().length());
		resp.getWriter().write(sb.toString());
	}
}
