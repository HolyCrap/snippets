package com.luxoft.courses.javaWeb;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = -8445908045156931706L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		HttpSession session = req.getSession(false);
		if (session == null || session.getAttribute("username") == null) {
			resp.sendRedirect(ServletUtils.APPPATH + ServletUtils.INDEX);
		}
		else {
			req.getRequestDispatcher(ServletUtils.WELCOME).forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String name = req.getParameter("name");
		String password = req.getParameter("password");
		Document doc;
		try {
			DocumentBuilder b = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			String param = (String) getServletContext().getInitParameter("users");
			File f = new File(getServletContext().getRealPath(param));
			doc = b.parse(f);
		} catch (SAXException | ParserConfigurationException e) {
			Errors.internalServerError(resp, "Error in parsing users.xml");
			return;
		}
		if (checkUser(doc,name,password)) {
			req.getSession().setAttribute("username",name);
			req.getRequestDispatcher(ServletUtils.WELCOME).forward(req, resp);
		}
		else {
			req.setAttribute("message", "Invalid login or password");
			req.getRequestDispatcher(ServletUtils.INDEX).forward(req, resp);
		}
	}	
	
	private boolean checkUser(Document doc, String name, String pass) {
		NodeList nl = null;
		StringBuilder pattern = new StringBuilder();
		pattern.append("//user[@name='");
		pattern.append(name);
		pattern.append("' and @password='");
		pattern.append(pass);
		pattern.append("']");
		try {	
			XPathExpression expr = XPathFactory.newInstance().
					newXPath().compile(pattern.toString());
			nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		if (nl.getLength() > 0) return true;
		else return false;
	}
}
