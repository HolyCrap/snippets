package com.luxoft.courses.javaWeb;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.luxoft.courses.javaWeb.exceptions.ParseException;

public class NameAgeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1793301084682648261L;
	private static final ConcurrentMap<String, Integer> container = new ConcurrentHashMap<>();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		ReadResult r;
		try {
			r = readValues(req);
		} catch (IllegalArgumentException e) {
			Errors.internalServerError(resp, "{\"error\": \"Illegal parameters\"}");
			return;
		}
		if (container.containsKey(r.name)) {
			Errors.internalServerError(resp, "{\"error\": \"Name " + r.name
					+ " already exists, current age: " + container.get(r.name)
					+ "\"}");
			return;
		}	
		container.put(r.name, r.age);
		resp.setStatus(HttpServletResponse.SC_CREATED);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Map<String, String[]> parameters = req.getParameterMap();
		if (parameters.isEmpty()) {
			ReadResult r;
			try {
				r = readValues(req);
			} catch (IllegalArgumentException e) {
				Errors.internalServerError(resp, "{\"error\": \"Illegal parameters\"}");
				return;
			}
			if (!container.containsKey(r.name)) {
				Errors.internalServerError(resp, "{\"error\": \"Name " + r.name
						+ " does not exist\"}");
				return;
			}
			container.put(r.name, r.age);
		}
		else {
			container.put(parameters.get("name")[0], new Integer(parameters.get("age")[0]));
		}
		resp.setStatus(HttpServletResponse.SC_ACCEPTED);
	}


	private boolean containCheck(Map<String, String> parameters) {
		return !parameters.keySet().contains("name")
				|| !parameters.keySet().contains("age");
	}

	private boolean nullCheck(Map<String, String> parameters) {
		return parameters.get("name").equals("null")
				|| parameters.get("age").equals("null");
	}

	private ReadResult readValues(HttpServletRequest req) throws IOException {
		Map<String, String> parameters = null;
		try {
			parameters = ServletUtils.parseBody(ServletUtils.getBody(req));
		} catch (ParseException e) {
			
			throw new IllegalArgumentException();
		}
		if (containCheck(parameters) || nullCheck(parameters)) {
			throw new IllegalArgumentException();
		}
		String name = parameters.get("name");
		Integer age;
		try {
			age = new Integer(parameters.get("age"));
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException();
		}
		return new ReadResult(name, age);
	}

	private class ReadResult {
		String name;
		int age;

		ReadResult(String name, int age) {
			this.name = name;
			this.age = age;
		}
	}
}
