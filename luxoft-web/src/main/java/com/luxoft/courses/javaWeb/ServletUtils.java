package com.luxoft.courses.javaWeb;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.luxoft.courses.javaWeb.exceptions.ParseException;

public final class ServletUtils {
	public static final String WELCOME = "/WEB-INF/welcome.jsp";
	public static final String INDEX = "/index.jsp";
	public static final String APPPATH = "/myFirstWebApp";
	
	private ServletUtils() {
		
	}
	
	public static String getBody(HttpServletRequest request) throws IOException {

	    StringBuilder sb = new StringBuilder();
	    BufferedReader reader = request.getReader();
	    int bytesread = -1;
	    char[] charBuffer = new char[128];
	    while ((bytesread = reader.read(charBuffer)) > 0) {
            sb.append(charBuffer, 0, bytesread);
        }
	    reader.close();
	    return sb.toString();
	}
	
	public static Map<String, String> parseBody(String body) throws ParseException {
		Map<String, String> map = new HashMap<>();
		String[] pairs = body.split("&");
		for (String pair: pairs) {
			String[] kv = pair.split("=");
			if (kv.length!=2) throw new ParseException();
			map.put(kv[0], kv[1]);
		}
		return map;
	}
}
