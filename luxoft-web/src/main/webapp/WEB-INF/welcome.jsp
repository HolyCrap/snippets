<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<meta content="no-cache" />
<style>
div {
	width: 100%
}
</style>
<title>Welcome!</title>
</head>
<body>
	<div>
		<h1>
			Hello
			<%=session.getAttribute("username")%></h1>
	</div>
	<div>
		<input type="button" onclick="location.href='/myFirstWebApp/logout'" value ="Logout" />
	</div>
</body>
</html>