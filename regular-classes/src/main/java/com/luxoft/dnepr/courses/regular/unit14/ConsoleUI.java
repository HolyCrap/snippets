package com.luxoft.dnepr.courses.regular.unit14;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.luxoft.dnepr.courses.regular.unit14.commands.*;

public class ConsoleUI implements Runnable, UI{
	private Controller controller = new Controller();
	private Scanner scanner = new Scanner(System.in);
	private volatile boolean finished = false;
	private Map<String, Command> commandsList = new HashMap<>();
	
	public ConsoleUI() {
		commandsList.put("exit", new ExitCommand(controller, this));
		commandsList.put("help", new HelpCommand(controller, this));
		commandsList.put("y", new OnReplyCommand(controller, this));
		commandsList.put("n", new OnReplyCommand(controller, this));
	}

	public void run() {
		String word;
		String answer;
		printGreeting();
		while(!finished && !Thread.interrupted()) {
			word = controller.getRandomWord();
			System.out.println(MessageTemplates.getQuestion(word));
			answer = scanner.next();
			procedeCommand(word,answer);
		}
	}

	public static void main(String[] args) {
		new Thread(new ConsoleUI()).start();
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}	
	
	private void printGreeting() {
		System.out.println("Lingustic analizator v1, autor Tushar Brahmacobalol");
		System.out.println("Type 'help' to view help, 'exit' to finish");
	}
	
	private void procedeCommand(String word, String answer) {
		Command command = commandsList.get(answer);
		if (command != null) {
			System.out.println(command.action(word, answer));
		} else {
			System.out.println(MessageTemplates.ERROR_READING);
			procedeCommand(word, scanner.next());
		}
	}
}
