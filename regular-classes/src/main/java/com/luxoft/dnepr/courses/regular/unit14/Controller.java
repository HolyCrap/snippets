package com.luxoft.dnepr.courses.regular.unit14;

public class Controller {
	private Vocabulary vocabulary;
	private Words words;
	
	public Controller(Vocabulary vocabulary, Words words) {
		super();
		this.vocabulary = vocabulary;
		this.words = words;
	}
	
	public Controller() {
		vocabulary = new Vocabulary(TextFileUtils.filePath);
		words = new Words();
	}
	
	public void procedeWord(String word, String userAnswer) {
		words.addAWord(word, isKnown(userAnswer));
	}
	
	private boolean isKnown(String userAnswer) {
		switch (userAnswer.toLowerCase()) {
		case "y":
			return true;
		case "n":
			return false;
		default: 
			throw new IllegalArgumentException("Wrong userAnswer format");
		}
	}
	
	public String getRandomWord() {
		return vocabulary.random();
	}
	
	public int getStatistics() {
		return words.getWordStatistics(vocabulary.getSize());
	}
}
