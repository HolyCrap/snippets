package com.luxoft.dnepr.courses.regular.unit14;

public class MessageTemplates {
	public static final String HELP = "Lingustic analizator v1, autor Tushar "
			+ "Brahmacobalol for help type help, answer y/n question, your "
			+ "english knowlege will give you";
	public static final String ERROR_READING = "Error while reading answer, please answer again";
	
	public static String getStatisticsMessage(int statistics) {
		return "Your estimated vocabulary is " + statistics + " words";
	}
	
	public static String getQuestion(String word){
		return "Do you know translation of word '" + word + "'? (y/n)";
	}
}
