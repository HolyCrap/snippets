package com.luxoft.dnepr.courses.regular.unit14;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public final class TextFileUtils {
	private static final int MIN_WORD_LENGTH = 3;
	public static final String filePath = "sonnets.txt";
	
	private TextFileUtils(){
		
	}
	
	public static String readFile(String fileName) throws IOException {
		Reader reader = new InputStreamReader
				(TextFileUtils.class.getResourceAsStream(fileName));
		char[] buffer = new char[64];
		int read  = 0;
		StringBuilder builder = new StringBuilder();
		while ((read = reader.read(buffer)) > 0) {
			builder.append(buffer, 0, read);
		}
		reader.close();
		return builder.toString();
	}
	
	public static Set<String> tokenizeStringToSet(String string) {
		Set<String> result = new HashSet<>();
		StringTokenizer tokenizer = new StringTokenizer(string);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken().
					replaceAll("[^a-zA-Zа-яА-ЯёЁ']", "");
			if (token.length() > MIN_WORD_LENGTH) {
				result.add(token.toLowerCase());
			}
		}
		return result;
	}
}
