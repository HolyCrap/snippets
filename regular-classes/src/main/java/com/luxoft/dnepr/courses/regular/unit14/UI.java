package com.luxoft.dnepr.courses.regular.unit14;

public interface UI {
	public boolean isFinished();
	public void setFinished(boolean finished);
}
