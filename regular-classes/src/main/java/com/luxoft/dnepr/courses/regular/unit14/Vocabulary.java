package com.luxoft.dnepr.courses.regular.unit14;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Collections;

public class Vocabulary {
	private Set<String> vocabulary = new HashSet<String>();
	private List<String> vocabularyAsList;

	public Vocabulary(String fileName) {
		try{
			String fileText = TextFileUtils.readFile(fileName);
			vocabulary = TextFileUtils.tokenizeStringToSet(fileText);
			vocabularyAsList = new ArrayList<>(vocabulary);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String random() {
		return vocabularyAsList.get((int)
				( Math.random() * vocabularyAsList.size()));
	}
	
	public int getSize() {
		return vocabulary.size();
	}
	
	public Set<String> getVocabulary() {
		return Collections.unmodifiableSet(vocabulary);
	}
}
