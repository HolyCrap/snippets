package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Words {
	private final Set<String> knownWords = new HashSet<>();
	private final Set<String> unknownWords = new HashSet<>();

	public int getWordStatistics(int totalWords) {
		return totalWords * (knownWords.size() + 1) / 
				(knownWords.size() + unknownWords.size() + 1);
	}

	public void addAWord(String word, boolean isKnown) {
		if (isKnown) {
			knownWords.add(word);
		} else {
			unknownWords.add(word);
		}
	}

	public Set<String> getKnownWords() {
		return Collections.unmodifiableSet(knownWords);
	}

	public Set<String> getUnknownWords() {
		return Collections.unmodifiableSet(unknownWords);
	}
}
