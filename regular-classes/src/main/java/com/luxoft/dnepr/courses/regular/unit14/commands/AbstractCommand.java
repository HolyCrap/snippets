package com.luxoft.dnepr.courses.regular.unit14.commands;

import com.luxoft.dnepr.courses.regular.unit14.*;

public abstract class AbstractCommand implements Command{
	protected String command;
	protected Controller controller;
	protected UI ui;
	
	public AbstractCommand(Controller controller, UI ui) {
		super();
		this.controller = controller;
		this.ui = ui;
	}

	public abstract String action(String word, String answer);

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}
