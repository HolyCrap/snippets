package com.luxoft.dnepr.courses.regular.unit14.commands;

public interface Command {
	public abstract String action(String word, String answer);
}
