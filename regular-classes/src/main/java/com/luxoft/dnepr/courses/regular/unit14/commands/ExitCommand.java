package com.luxoft.dnepr.courses.regular.unit14.commands;

import com.luxoft.dnepr.courses.regular.unit14.ConsoleUI;
import com.luxoft.dnepr.courses.regular.unit14.Controller;
import com.luxoft.dnepr.courses.regular.unit14.MessageTemplates;

public class ExitCommand extends AbstractCommand{

	public ExitCommand(Controller controller, ConsoleUI ui) {
		super(controller, ui);
	}

	@Override
	public String action(String word, String answer) {
		ui.setFinished(true);
		return MessageTemplates.getStatisticsMessage(controller.getStatistics());
	}

}
