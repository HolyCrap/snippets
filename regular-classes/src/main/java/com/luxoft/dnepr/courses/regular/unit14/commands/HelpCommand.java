package com.luxoft.dnepr.courses.regular.unit14.commands;

import com.luxoft.dnepr.courses.regular.unit14.ConsoleUI;
import com.luxoft.dnepr.courses.regular.unit14.Controller;
import com.luxoft.dnepr.courses.regular.unit14.MessageTemplates;

public class HelpCommand extends AbstractCommand{

	public HelpCommand(Controller controller, ConsoleUI ui) {
		super(controller, ui);
		this.command = "help";
	}

	@Override
	public String action(String word, String answer) {
		return MessageTemplates.HELP;
	}

}
