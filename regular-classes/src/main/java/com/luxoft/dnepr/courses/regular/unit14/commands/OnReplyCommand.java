package com.luxoft.dnepr.courses.regular.unit14.commands;

import com.luxoft.dnepr.courses.regular.unit14.ConsoleUI;
import com.luxoft.dnepr.courses.regular.unit14.Controller;

public class OnReplyCommand extends AbstractCommand{

	public OnReplyCommand(Controller controller, ConsoleUI ui) {
		super(controller, ui);
	}

	@Override
	public String action(String word, String answer) {
		controller.procedeWord(word, answer);
		return "";
	}

}
