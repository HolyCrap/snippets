package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {
	private boolean nonAlcoholic;

	Beverage(String code, String name, double price, boolean nonAlcoholic) {
		super(code, name, price);
		this.nonAlcoholic = nonAlcoholic;
	}

	public boolean isNonAlcoholic() {
		return nonAlcoholic;
	}

	public void setNonAlcoholic(boolean nonAlcoholic) {
		this.nonAlcoholic = nonAlcoholic;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = PRIME * result + (nonAlcoholic ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Beverage other = (Beverage) obj;
		if (nonAlcoholic != other.nonAlcoholic)
			return false;
		return true;
	}
}
