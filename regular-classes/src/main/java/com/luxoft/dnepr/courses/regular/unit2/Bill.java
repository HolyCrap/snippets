package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
	private List<CompositeProduct> billList;
	
	public Bill () {
		billList = new ArrayList<CompositeProduct>();
	}

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     * @param product new product
     */
    public void append(Product product) {
        for (CompositeProduct p: billList) {
        	if (p.getChild().equals(product)) {
        		p.add(product);
        		return;
        	}
        }
    	CompositeProduct temp = new CompositeProduct();
    	temp.add(product);
    	billList.add(temp);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     * @return
     */
    public double summarize() {
        double totalCost = 0;
        for (Product p: billList) {
        	totalCost += p.getPrice();
        }
        return totalCost;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     * @return
     */
    public List<Product> getProducts() {
    	ArrayList<Product> temp = new ArrayList<Product>();
    	temp.addAll(billList);
    	Collections.sort(temp, new ProductComparator());
        return temp;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize() ;
    }

	private class ProductComparator implements Comparator<Product> {
		private static final int DELTA = 100;

		@Override
		public int compare(Product o1, Product o2) {
			return (int) (DELTA*(o2.getPrice()-o1.getPrice()));
		}
		
	}
}
