package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct {
	private Date publicationDate;
	
	Book(String code, String name, double price, Date publicationDate) {
		super(code, name, price);
		this.publicationDate = publicationDate;
	}
	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}
	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = PRIME * result
				+ ((publicationDate == null) ? 0 : publicationDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (publicationDate == null) {
			if (other.publicationDate != null)
				return false;
		} else if (!publicationDate.equals(other.publicationDate))
			return false;
		return true;
	}
	
	@Override
	public Product clone() throws CloneNotSupportedException {
		return new Book(getCode(),getName(),getPrice(),(Date) publicationDate.clone());
	}
}
