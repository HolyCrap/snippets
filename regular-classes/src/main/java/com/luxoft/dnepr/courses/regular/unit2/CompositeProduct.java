package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     * @return product code
     */
    @Override
    public String getCode() {
        if (!childProducts.isEmpty()) {
        	return childProducts.get(0).getCode();
        }
        else {
        	return null;
        }
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     * @return product name
     */
    @Override
    public String getName() {
    	if (!childProducts.isEmpty()) {
        	return childProducts.get(0).getName();
        }
        else {
        	return null;
        }
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     * @return total price of child products
     */
    @Override
    public double getPrice() {
    	double totalPrice = 0;
    	for (Product p: childProducts) {
    		totalPrice += p.getPrice();
    	}
        switch (childProducts.size()) {
      //default is also good, but this excludes multiplying
        case 0: return 0;	
        case 1: return totalPrice;
        case 2: return totalPrice*0.95;
        default: return totalPrice*0.9;
        }
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
    
    public Product getChild() {
    	if (!childProducts.isEmpty()) {
        	return childProducts.get(0);
        }
        else {
        	return null;
        }
    }
}
