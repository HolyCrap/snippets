package com.luxoft.dnepr.courses.regular.unit3;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import static com.luxoft.dnepr.courses.regular.unit3.BankUtils.*;
import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

public class Bank implements BankInterface {
	private Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();

	public Bank(String expectedJavaVersion) {
		//version launched fully depends on users
		if (expectedJavaVersion.length()>System.getProperty("java.version").length())
			throw new IllegalJavaVersionError(
					System.getProperty("java.version"), expectedJavaVersion,
					"Wrong java version");
		String mustBeCut = System.getProperty("java.version").substring(0, 
				expectedJavaVersion.length());
		if (!expectedJavaVersion.equals(mustBeCut)) {
			throw new IllegalJavaVersionError(
					System.getProperty("java.version"), expectedJavaVersion,
					"Wrong java version");
		}
	}

	@Override
	public Map<Long, UserInterface> getUsers() {
		return users;
	}

	@Override
	public void setUsers(Map<Long, UserInterface> users) {
		this.users = users;
	}

	@Override
	public void makeMoneyTransaction(Long fromUserId, Long toUserId,
			BigDecimal amount) throws NoUserFoundException,
			TransactionException {
		if (fromUserId == null)
			throw new InputMismatchException("FromUserID is null");
		if (toUserId == null)
			throw new InputMismatchException("ToUserID is null");
		if (amount == null)
			throw new InputMismatchException("Amount is null");
		containCheck(fromUserId);
		containCheck(toUserId);
		WalletInterface from = users.get(fromUserId).getWallet();
		WalletInterface to = users.get(toUserId).getWallet();
		try {
			from.checkWithdrawal(amount);
		} catch (WalletIsBlockedException blocked) {
			throw new TransactionException(userTemplate(fromUserId)
					+ " " +blocked.getMessage());
		//will be checked below, just for keeping priority on WalletIsBlocked	
		} catch (InsufficientWalletAmountException e) {
		}
		try {
			to.checkTransfer(amount);
		} catch (WalletIsBlockedException blocked) {
			throw new TransactionException(userTemplate(toUserId)
					+ " " +blocked.getMessage());
		//will be checked below, just for keeping priority on WalletIsBlocked	
		} catch (LimitExceededException e) {
		}
		try {
			from.checkWithdrawal(amount);
		} catch (InsufficientWalletAmountException ins) {
				InsufficientWalletAmountExceptionToTransaction(fromUserId, ins);
		} catch (WalletIsBlockedException e) {
			//already checked
		}
		try {
			to.checkTransfer(amount);
		} catch (WalletIsBlockedException e) {
			//already checked
		} catch (LimitExceededException e) {
			LimitExceededExceptionToTransaction(toUserId, e);
		}
		from.withdraw(amount);
		to.transfer(amount);
	}

	private void containCheck(Long id) throws NoUserFoundException {
		if (id == null) {
			throw new InputMismatchException("Id is null");
		}
		if (!users.containsKey(id)) {
			throw new NoUserFoundException(id, "User with ID'" + id + "' was not found");
		}
	}
	
	private String userTemplate(Long key) {
		return "User '" + users.get(key).getName()+ "'";
	}
	
	private void LimitExceededExceptionToTransaction(Long id, 
			LimitExceededException e) throws TransactionException {
		String message = userTemplate(id)+ " wallet limit exceeded ("
				+ decimalOut(e.getAmountInWallet()) + " + "
				+ decimalOut(e.getAmountToTransfer()) + " > "
				+ decimalOut(users.get(id).getWallet().getMaxAmount())
				+ ")";
		throw new TransactionException(message);
	}
	
	private void InsufficientWalletAmountExceptionToTransaction(Long id,
			InsufficientWalletAmountException e) throws TransactionException {
		String message = userTemplate(id)+" has insufficient funds ("
				+ decimalOut(e.getAmountInWallet()) + " < "
				+ decimalOut(e.getAmountToWithdraw()) + ")";
		throw new TransactionException(message);
	}
}
