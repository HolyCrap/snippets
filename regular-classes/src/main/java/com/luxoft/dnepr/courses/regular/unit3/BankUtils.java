package com.luxoft.dnepr.courses.regular.unit3;

import java.math.BigDecimal;

public final class BankUtils {
	private BankUtils() {
		
	}
	
	public static String decimalOut(BigDecimal val) {
		return val.setScale(2,BigDecimal.ROUND_HALF_UP).toPlainString();
	}
}
