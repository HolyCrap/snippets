package com.luxoft.dnepr.courses.regular.unit3;

import java.util.InputMismatchException;

public class User implements UserInterface {
	private Long id;
	private String name;
	private WalletInterface wallet;

	public User(Long id, String name, WalletInterface wallet) {
		setId(id);
		setName(name);
		setWallet(wallet);
	}

	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		if (id == null)
			throw new InputMismatchException("Id is null");
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null)
			throw new InputMismatchException("Name is null");
		this.name = name;
	}

	public WalletInterface getWallet() {
		return wallet;
	}

	public void setWallet(WalletInterface wallet) {
		if (wallet == null)
			throw new InputMismatchException("Wallet is null");
		this.wallet = wallet;
	}

}
