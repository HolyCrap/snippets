package com.luxoft.dnepr.courses.regular.unit3;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

public class Wallet implements WalletInterface{
	private long id;
	private BigDecimal amount;
	private WalletStatus status;
	private BigDecimal maxAmount;
	
	public Wallet(long id, BigDecimal amount, WalletStatus status,
			BigDecimal maxAmount) {
		setId(id);
		setAmount(amount);
		setStatus(status);
		setMaxAmount(maxAmount);
	}

	public Wallet() {
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public BigDecimal getAmount() {
		return amount;
	}

	@Override
	public void setAmount(BigDecimal amount){
		if (amount == null)
			throw new InputMismatchException("Amount is null");
		this.amount = amount;
	}

	@Override
	public WalletStatus getStatus() {
		return status;
	}

	@Override
	public void setStatus(WalletStatus status) {
		if (status == null) 
			throw new InputMismatchException("Status is null");
		this.status = status;
	}

	@Override
	public BigDecimal getMaxAmount() {
		return maxAmount;
	}

	@Override
	public void setMaxAmount(BigDecimal maxAmount) {
		if (maxAmount == null) 
			throw new InputMismatchException("Max amount is null");
		this.maxAmount = maxAmount;
	}

	@Override
	public void checkWithdrawal(BigDecimal amountToWithdraw)
			throws WalletIsBlockedException, InsufficientWalletAmountException {
		if (amountToWithdraw == null) 
			throw new InputMismatchException("Amount to withdraw is null");
		checkBlocked();
		if (amount.compareTo(amountToWithdraw) < 0)
			throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, 
					"insufficient funds");
	}

	@Override
	public void withdraw(BigDecimal amountToWithdraw) {
		amount = amount.subtract(amountToWithdraw);
	}

	@Override
	public void checkTransfer(BigDecimal amountToTransfer)
			throws WalletIsBlockedException, LimitExceededException {
		if (amountToTransfer == null) {
			throw new InputMismatchException("Amount to transfer is null");
		}
		checkBlocked();
		if (amount.add(amountToTransfer).compareTo(maxAmount) > 0) {
			throw new LimitExceededException(id, amountToTransfer, amount, 
					"limit exceeded");
		}
	}

	@Override
	public void transfer(BigDecimal amountToTransfer) {
		amount = amount.add(amountToTransfer);
	}
	
	private void checkBlocked() throws WalletIsBlockedException {
		if (status == WalletStatus.BLOCKED) 
			throw new WalletIsBlockedException(id, "wallet is blocked");
	}
}
