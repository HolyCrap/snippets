package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

public class EqualSet<E> implements Set<E>, Cloneable{
	private ArrayList<E> inner = new ArrayList<E>();
	
	public EqualSet() {
		super();
	}

	public EqualSet(Collection<? extends E> collection) {
		argCollectionCheck(collection);
		this.addAll(collection);
	}

	@Override
	public boolean add(E value) {
		if (!inner.contains(value)) {
			inner.add(value);
			return true;
		}
		else return false;
	}

	@Override
	public boolean addAll(Collection<? extends E> collection) {
		argCollectionCheck(collection);
		int prevSize = inner.size();
		for (E val: collection) {
			this.add(val);
		}
		if (inner.size() > prevSize) 
			return true;
		else return false;
	}

	@Override
	public void clear() {
		inner.clear();
	}

	@Override
	public boolean contains(Object value) {
		if (inner.contains(value))
			return true;
		else return false;
	}

	@Override
	public boolean containsAll(Collection<?> collection) {
		argCollectionCheck(collection);
		if (inner.containsAll(collection)) 
			return true;
		else return false;
	}

	@Override
	public boolean isEmpty() {
		return inner.isEmpty();
	}

	@Override
	public Iterator<E> iterator() {
		return inner.iterator();
	}

	@Override
	public boolean remove(Object value) {
		if (inner.contains(value)) {
			inner.remove(value);
			return true;
		}
		else return false;
	}

	@Override
	public boolean removeAll(Collection<?> collection) {
		argCollectionCheck(collection);
		int prevSize = inner.size();
		for (Object elem: collection) {
			inner.remove(elem);
		}
		if (inner.size() < prevSize) 
			return true;
		else return false;
	}

	@Override
	public boolean retainAll(Collection<?> collection) {
		argCollectionCheck(collection);
		ArrayList<E> tempList = new ArrayList<E>();
		int prevSize = inner.size();
		for (E elem: inner) {
			if (collection.contains(elem))
				tempList.add(elem);
		}
		inner = tempList;
		if (inner.size() < prevSize) 
			return true;
		else return false;
	}

	@Override
	public int size() {
		return inner.size();
	}

	@Override
	public Object[] toArray() {
		return inner.toArray();
	}

	@Override
	public <T> T[] toArray(T[] array) {
		return inner.toArray(array);
	}
	
	private void argCollectionCheck(Object arg) {
		if (arg == null)
			throw new NullPointerException("Argument collection is null");
	}

	@Override
	public int hashCode() {
		int result = 0;
		for (E elem: inner) {
			if (elem != null)
				result += elem.hashCode();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Set))
			return false;
		Set<E> other = (Set<E>) obj;
		if (this.size() != other.size())
			return false;
		if (!this.containsAll(other))
			return false;
		if (!other.containsAll(this))
			return false;
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() {
		EqualSet<E> clone = null;
		try {
			clone = (EqualSet<E>) super.clone();
			clone.inner = (ArrayList<E>) inner.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return clone;
	}
}
