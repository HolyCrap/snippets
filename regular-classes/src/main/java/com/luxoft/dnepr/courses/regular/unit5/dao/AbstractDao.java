package com.luxoft.dnepr.courses.regular.unit5.dao;

import java.util.Map;

import com.luxoft.dnepr.courses.regular.unit5.exception.*;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

public class AbstractDao<E extends Entity> implements IDao<E> {
	private Map<Long, E> map;
	
	/**
	 * @param storage - storage of entity type. Is kept in StorageHolder class.
	 */
	public AbstractDao(EntityStorage<E> storage) {
		if (storage == null)
			throw new IllegalArgumentException("Storage is null");
		map = storage.getEntities();
	}
	
	@Override
	public E save(E e) throws UserAlreadyExist {
		if (e.getId() == null) {
			e.setId(getMaxKeyPlusOne());
		}
		if (map.containsKey(e.getId()))
			throw new UserAlreadyExist(e.getId());
		map.put(e.getId(), e);
		return e;
	}

	@Override
	public E update(E e) throws UserNotFound {
		if (e.getId() == null || !map.containsKey(e.getId()))
			throw new UserNotFound(e.getId());
		map.put(e.getId(), e);
		return e;
	}

	@Override
	public E get(long id) {
		if (!map.containsKey(Long.valueOf(id)))
			return null;	
		return  map.get(Long.valueOf(id));
	}

	@Override
	public boolean delete(long id) {
		if (map.containsKey(Long.valueOf(id))) {
			map.remove(Long.valueOf(id));
			return true;
		}
		else return false;		
	}

	private Long getMaxKeyPlusOne() {
		Long currentMax = 0L;
		for (Long val: map.keySet()) {
			if (val.compareTo(currentMax) > 0) currentMax = val;
		}
		return Long.valueOf(currentMax.longValue() + 1L);
	}
}
