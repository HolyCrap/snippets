package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.StorageHolder;

//just for tests, if they exist
public class RedisDaoImpl extends AbstractDao<Redis>{
	public RedisDaoImpl() {
		super(StorageHolder.getRedisStorage());
	}
}
