package com.luxoft.dnepr.courses.regular.unit5.exception;

@SuppressWarnings("serial")
public class UserAlreadyExist extends RuntimeException {
	private Long existId;
	public UserAlreadyExist(Long id) {
		super();
		existId = id;
	}
	public Long getExistId() {
		return existId;
	}
}
