package com.luxoft.dnepr.courses.regular.unit5.exception;

@SuppressWarnings("serial")
public class UserNotFound extends RuntimeException {
	public Long notFoundId;

	public UserNotFound(Long notFoundId) {
		super();
		this.notFoundId = notFoundId;
	}

	public Long getNotFoundId() {
		return notFoundId;
	}
}
