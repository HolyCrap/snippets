package com.luxoft.dnepr.courses.regular.unit5.storage;

import java.util.HashMap;
import java.util.Map;

public class EntityStorage<T> {
    private final Map<Long, T> entities;
    
    public EntityStorage() {
    	entities = new HashMap<>();
    }
    public Map<Long, T> getEntities() {
        return entities;
    }
}