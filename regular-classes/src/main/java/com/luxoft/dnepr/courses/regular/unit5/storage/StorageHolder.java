package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.*;

public class StorageHolder {
	
	private StorageHolder() {
		
	}
	private static EntityStorage<Redis> redisStorage = new EntityStorage<>();
	private static EntityStorage<Employee> employeeStorage = new EntityStorage<>();
	public static EntityStorage<Redis> getRedisStorage() {
		return redisStorage;
	}
	public static EntityStorage<Employee> getEmployeeStorage() {
		return employeeStorage;
	}
}
