package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

public class FamilyTreeImpl implements FamilyTree {
	
	private static final long serialVersionUID = 3057396458981676327L;
	private Person root;
	private transient long creationTime;
	
	private FamilyTreeImpl(Person root, long creationTime) {
		this.root = root;
		this.creationTime = creationTime;
	}
	
	public static FamilyTree create(Person root) {
		return new FamilyTreeImpl(root, System.currentTimeMillis());
	}
	
	@Override
	public Person getRoot() {
		return root;
	} 
	
	@Override
	public long getCreationTime() {
		return creationTime;
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		//out.defaultWriteObject();
		OutputStreamWriter out2 = new OutputStreamWriter(out,"UTF-8");
		out2.write("{\"root\":");
		if (root == null) {
			out2.write("null}");
		}
		else {
			if (root instanceof PersonImpl) {
				out2.write(PersonImpl.toJSON(root));
				out2.write("}");
			}
		}
		out2.flush();
	}
	
	private void readObject(ObjectInputStream in) throws IOException {
		InputStreamReader reader = new InputStreamReader(in);
		StringBuilder sb = new StringBuilder();
		char[] buffer = new char[32];
		int read = 0;
		while (true) {
			read = reader.read(buffer);
			if (read == -1) break;
			sb.append(buffer,0,read);
		}
		String json = sb.toString();
		json = json.replaceAll("\"", "");
		int startIndex = json.indexOf("root:")+5;
		if (startIndex == 0) 
			throw new IOException("JSON is invalid");
		String rootPersonString = json.substring(
				startIndex, json.length()-1);
		root = PersonImpl.fromJSON(rootPersonString);
		creationTime = System.currentTimeMillis();
	}
}
