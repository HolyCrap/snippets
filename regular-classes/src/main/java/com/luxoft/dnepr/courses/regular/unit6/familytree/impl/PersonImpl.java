package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import java.io.IOException;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

public class PersonImpl implements Person {
	private static final long serialVersionUID = 3450403185146609550L;
	private String name;
	private String ethnicity;
	private Person father;
	private Person mother;
	private Gender gender;
	private int age;

	public PersonImpl(String name, String ethnicity, Person father,
			Person mother, Gender gender, int age) {
		super();
		this.name = name;
		this.ethnicity = ethnicity;
		this.father = father;
		this.mother = mother;
		this.gender = gender;
		this.age = age;
	}

	public PersonImpl() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getEthnicity() {
		return this.ethnicity;
	}
	
	@Override
	public Person getFather() {
		return this.father;
	}

	@Override
	public Person getMother() {
		return this.mother;
	}

	@Override
	public Gender getGender() {
		return this.gender;
	}

	@Override
	public int getAge() {
		return this.age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public void setFather(Person father) {
		this.father = father;
	}

	public void setMother(Person mother) {
		this.mother = mother;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("{name: ");
		b.append(this.name);
		b.append(" ethnicity: ");
		b.append(this.ethnicity);
		b.append(" gender: ");
		b.append(this.gender);
		b.append(" age: ");
		b.append(this.age);
		b.append(" father: ");
		b.append(this.father);
		b.append(" mother: ");
		b.append(this.mother);
		b.append("}");
		return b.toString();
	}
	
	public static String toJSON(Person person) {
		StringBuilder sb = new StringBuilder();
		if (person == null) {
			sb.append("null");
		}
		else {
			sb.append("{");
			sb.append("\"name\":");
			appendString(sb,person.getName());
			sb.append(",\"gender\":");
			appendString(sb, person.getGender().toString());
			sb.append(",\"ethnicity\":");
			appendString(sb, person.getEthnicity());
			sb.append(",\"age\":");
			sb.append(person.getAge());
			sb.append(",\"father\":");
			sb.append(toJSON(person.getFather()));
			sb.append(",\"mother\":");
			sb.append(toJSON(person.getMother()));
			sb.append("}");
		}
		return sb.toString();
	}
	
	//deprecated, left if second method fails
	public static Person fromJSON(String json) throws IOException {
		if (json.equals("null")) return null;
		if (json.charAt(0)!='{' || json.charAt(json.length()-1)!='}')
			throw new IOException("JSON is invalid");
		PersonImpl p = new PersonImpl();
		//numbers added to indexes are lengths of these strings
		//made for determining the position of field value start
		int nameIndex = json.indexOf("name:")+5;
		int genderIndex = json.indexOf("gender:")+7;
		int ethnicityIndex = json.indexOf("ethnicity:")+10;
		int ageIndex = json.indexOf("age:")+4;
		int fatherIndex = json.indexOf("father:")+7;
		int motherIndex = fatherIndex;
		int openedCount = 0;
		//checks if father is null
		if (json.charAt(motherIndex) != 'n') {
			for (int i = motherIndex; i < json.length(); i++) {
				switch (json.charAt(i)) {
					case '{': openedCount++; break;
					case '}': openedCount--; break;
				}
				if (openedCount == 0) {
					motherIndex = i+1;
					break;
				}
			}
			if (openedCount != 0 || motherIndex == fatherIndex) {
				throw new IOException("Wrong brackets");
			}
		}
		else {
			motherIndex += 4;
		}
		//minus length of strings and comma
		p.setName(json.substring(nameIndex, genderIndex - 8));
		String gender = json.substring(genderIndex, ethnicityIndex-11);
		switch (gender) {
			case "MALE": p.setGender(Gender.MALE); break;
			case "FEMALE":p.setGender(Gender.FEMALE); break;
			case "null": p.setGender(null); break;
			default: throw new IOException("Invalid gender");
		}
		p.setEthnicity(json.substring(ethnicityIndex, ageIndex-5));
		p.setAge(Integer.parseInt(json.substring(ageIndex, fatherIndex-8)));
		p.setFather(fromJSON(json.substring(fatherIndex, motherIndex)));
		p.setMother(fromJSON(json.substring(motherIndex+8, json.length()-1)));
		return p;
	}
	
	private static void appendString(StringBuilder sb, String s) {
		if (s == null) {
			sb.append("null");
		}
		else {
			sb.append("\"");
			sb.append(s);
			sb.append("\"");
		}
	}
}
