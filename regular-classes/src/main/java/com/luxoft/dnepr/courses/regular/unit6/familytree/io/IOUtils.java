package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.*;

import java.io.*;

public class IOUtils {
	
	private IOUtils() {
	}

	public static FamilyTree load(String filename) throws IOException {
		try (FileInputStream fis = new FileInputStream(filename)) {
			return load(fis);
		}
	}
	
	public static FamilyTree load(InputStream is) throws IOException {
		ObjectInputStream ois = new ObjectInputStream(is);
		try {
			return (FamilyTree) ois.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static void save(String filename, FamilyTree familyTree) 
			throws IOException {
		FileOutputStream out = new FileOutputStream(filename);
		save(out,familyTree);
		out.close();
	}
	
	public static void save(OutputStream os, FamilyTree familyTree) throws IOException {
		ObjectOutputStream out = new ObjectOutputStream(os);
		out.writeObject(familyTree);
	}
}
