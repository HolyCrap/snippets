package com.luxoft.dnepr.courses.regular.unit7;

import java.nio.file.Paths;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    private WordStorage wordStorage = new WordStorage();
    private FileSearcher searcher;
    private WordCounter counter;
    private int maxNumberOfThreads;

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
    	if (rootFolder == null)
    		throw new IllegalArgumentException("Root is null");
    	//found a relative symbol before absolute path
    	if (rootFolder.matches("/.:.*"))
    		rootFolder = rootFolder.substring(1,rootFolder.length());
        searcher = new FileSearcher(Paths.get(rootFolder));
        counter = new WordCounter(wordStorage, searcher);
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
    	Thread[] threads = new Thread[maxNumberOfThreads];
    	threads[0] = new Thread(searcher);
    	for (int i = 1; i < maxNumberOfThreads; i++) {
    		threads[i] = new Thread(counter);
    	}
    	for (Thread thr: threads) {
    		thr.start();
    	}
    	for (Thread thr: threads) {
    		try {
				thr.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
    	}
    	return new FileCrawlerResults(searcher.getAllFiles(),
    			wordStorage.getWordStatistics());
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        System.out.println(crawler.execute().getWordStatistics());
    }
}
