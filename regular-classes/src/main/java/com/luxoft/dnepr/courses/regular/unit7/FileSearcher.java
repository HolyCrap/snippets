package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/*
 * Producer: searches files in current directory and subdirectories
 */
public class FileSearcher implements Runnable{
	private Queue<File> files;
	private List<File> allFiles;
	private Path path;
	private static final PathMatcher matcher =
		    FileSystems.getDefault().getPathMatcher("glob:*.txt");
	private volatile boolean finished = false;

	public FileSearcher(Path path) {
		if (path == null) 
			throw new IllegalArgumentException("Path is null");
		files = new ConcurrentLinkedQueue<File>();
		allFiles = new LinkedList<File>();
		this.path = path;
	}
	
	private void search(Path path) {
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
		    for (Path file: stream) {
		        if (Files.isDirectory(file)) {
		        	search(file);
		        }
		        else {
		        	if (matcher.matches(file.getFileName())) {
		        		files.offer(file.toFile());
		        		allFiles.add(file.toFile());
		        	}
		        }
		    }
		} catch (IOException | DirectoryIteratorException x) {
		    // IOException can never be thrown by the iteration.
		    // In this snippet, it can only be thrown by newDirectoryStream.
		    System.err.println(x);
		}
	}

	@Override
	public void run() {
		search(path);
		finished = true;
	}
	
	public Queue<File> getFileStorage() {
		return this.files;
	}
	
	public synchronized boolean isDone() {
		return finished && files.isEmpty();
	}

	public List<File> getAllFiles() {
		return allFiles;
	}
}
