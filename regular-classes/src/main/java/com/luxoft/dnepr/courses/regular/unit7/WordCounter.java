package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Queue;
import java.util.Scanner;

/*
 * Consumer: counts words in files found by FileSearcher
 */
public class WordCounter implements Runnable{
	private WordStorage wordStorage;
	private  FileSearcher searcher;
	private Queue<File> fileStorage;
	
	public WordCounter(WordStorage wordStorage, FileSearcher searcher) {
		super();
		this.wordStorage = wordStorage;
		this.searcher = searcher;
		this.fileStorage =  searcher.getFileStorage();
	}
	private void count() throws InterruptedException {
		File current = null;
		while (!searcher.isDone()) {
			if ((current = fileStorage.poll()) != null) {				
				try (InputStreamReader reader = new InputStreamReader(
						new FileInputStream(current),"UTF-8")) {
					Scanner sc = new Scanner(reader);
					sc.useDelimiter("[^a-zA-Z0-9а-яА-ЯёЁ]+");
					while (sc.hasNext()) {
						String str = sc.next();
						 wordStorage.save(str);
					}
					sc.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	@Override
	public void run() {
		try {
			count();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
