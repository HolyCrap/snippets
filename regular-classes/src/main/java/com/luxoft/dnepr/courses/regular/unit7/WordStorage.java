package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.*;

/**
 * Represents word statistics storage.
 */
public class WordStorage {
	private final Map<String, AtomicInteger> storage;
	
	public WordStorage() {
		storage = new HashMap<String,AtomicInteger>();
	}
    /**
     * Saves given word and increments count of occurrences.
     * @param word
     */
    public synchronized void save(String word) {
    	AtomicInteger value = null;
    	if (word == null)
    		throw new IllegalArgumentException("Word is null");
        if ((value = storage.get(word)) != null) {
        	value.incrementAndGet();
        }
        else {
        	storage.put(word, new AtomicInteger(1));
        }
        
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(storage);
    }
}
