package com.luxoft.dnepr.courses.unit1;

public final class LuxoftUtils {
	
	private LuxoftUtils () {
		
	}
	
	public static String getMonthName (int monthOrder, String language) {
		try {
			switch (language) {
			case "en":
				switch (monthOrder) {
				case 1: return "January";
				case 2: return "February";
				case 3: return "March";
				case 4: return "April";
				case 5: return "May";
				case 6: return "June";
				case 7: return "July";
				case 8: return "August";
				case 9: return "September";
				case 10: return "October";
				case 11: return "November";
				case 12: return "December";
				default: return "Unknown Month";
				}
			case "ru":
				switch (monthOrder) {
				case 1: return "Январь";
				case 2: return "Февраль";
				case 3: return "Март";
				case 4: return "Апрель";
				case 5: return "Май";
				case 6: return "Июнь";
				case 7: return "Июль";
				case 8: return "Август";
				case 9: return "Сентябрь";
				case 10: return "Октябрь";
				case 11: return "Ноябрь";
				case 12: return "Декабрь";
				default: return "Неизвестный месяц";
				}
			default: return "Unknown Language";
			}
		} catch (NullPointerException ex) {
			return "Unknown Language";
		}
	}
	
	public static String binaryToDecimal(String binaryNumber) {
		
		if (binaryNumber == null) throw new IllegalArgumentException("Input is null");
		char[] binaryNumberArray = binaryNumber.toCharArray();
		int multiplier = 1;
		int resultNumber = 0;
		for (int i=binaryNumberArray.length-1; i>=0; i--) {
			switch (binaryNumberArray[i]) {
			case '0': 
				break;
			case '1':
				resultNumber += multiplier;
				break;
			default:
				return "Not binary";
			}
			multiplier *= 2;
		}
		return Integer.toString(resultNumber);
	}
	
	public static String decimalToBinary(String decimalNumber) {

		try {
			if (decimalNumber == null) throw new IllegalArgumentException("Input is null");
			int intNumber = Integer.parseInt(decimalNumber);
			String result = "";
			while (intNumber != 0) {
				result += intNumber % 2;
				intNumber = (int) intNumber / 2;
			}
			result = new StringBuilder(result).reverse().toString();
			return result;
		} catch (NumberFormatException ex) {
			return "Not decimal";
		}
	}
	
	public static int[] sortArray(int[] array, boolean asc) {
		
		if (array == null) throw new IllegalArgumentException("Input is null");
 		int[] newArray = array.clone();
		boolean flag = true;
		while (flag) {
			flag = false;
			for (int j = 0; j < newArray.length - 1; j++) {
				if (asc) {
					if (newArray[j] > newArray[j + 1]) {
						swap(newArray,j,j+1);
						flag = true;
					}
				}
				else {
					if (newArray[j] < newArray[j + 1]) {
						swap(newArray,j,j+1);
						flag = true;
					}
				}
			}
		}
		return newArray;
	}
	
	private static void swap(int[] array, int i, int j){
		int temp;
		temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
