package com.luxoft.dnepr.courses.unit2;

import java.util.*;
import com.luxoft.dnepr.courses.unit2.model.Figure;

public final class LuxoftUtils {

	private LuxoftUtils() {

	}

	public static String[] sortArray(String[] array, boolean asc) {
		if (array == null) {
			throw new IllegalArgumentException("Null argument");
		}
		for (String s : array) {
			if (s == null) {
				throw new IllegalArgumentException(
						"Null elements of array prohibited");
			}
			if (!s.matches("[a-zA-Z\\s]+")) {
				throw new IllegalArgumentException(
						"Must contain letters and whitespaces");
			}
		}
		String[] startArray = array.clone();
		String[] helper = new String[array.length];
		mergesort(startArray, helper, 0, helper.length - 1, asc);
		return startArray;
	}

	public static double wordAverageLength(String str) {
		stringWordCheck(str);
		String temp = "";
		int lengthContainer = 0;
		int wordsCounter = 0;
		for (char c : str.toCharArray()) {
			if (Character.isWhitespace(c)) {
				lengthContainer += temp.length();
				if (temp.length() != 0) {
					wordsCounter++;
				}
				temp = "";
			} else {
				temp += c;
			}
		}
		if (temp.length() > 0) {
			wordsCounter++;
			lengthContainer += temp.length();
		}
		return (double) lengthContainer / wordsCounter;
	}

	public static String reverseWords(String str) {
		stringWordCheck(str);
		String temp = "";
		String result = "";
		for (char c : str.toCharArray()) {
			if (Character.isWhitespace(c)) {
				result += new StringBuilder(temp).reverse().toString() + " ";
				temp = "";
			} else {
				temp += c;
			}
		}
		if (temp.length() > 0) {
			result += new StringBuilder(temp).reverse().toString();
		}
		return result;
	}

	public static char[] getCharEntries(String str) {
		stringWordCheck(str);
		str = str.replaceAll(" ", "");
		
		TreeMap<Character, Integer> charMap = new TreeMap<Character,Integer>();
		for (char c : str.toCharArray()) {
			if (charMap.containsKey(c)) {
				charMap.put(c, charMap.get(c).intValue() + 1);
			}
			else {
				charMap.put(c, 1);
			}
		}
		TreeSet<KeyValuePair> sortedSet = new TreeSet<KeyValuePair>();
		for (Map.Entry<Character, Integer> entry : charMap.entrySet()) {
			sortedSet.add(new KeyValuePair(entry.getKey(), entry.getValue()));
		}
		char[] resArray = new char[sortedSet.size()];
		int i = 0;
		for (KeyValuePair entry : sortedSet) {
			resArray[i] = entry.key;
			i++;
		}
		return resArray;
	}

	public static double calculateOverallArea(List<Figure> figures) {
		double result = 0;
		for (Figure f: figures) {
			result += f.getArea();
		}
		return result;
	}
	
	private static void mergesort(String[] arr, String[] help, int low,
			int high, boolean asc) {
		if (low < high) {
			int middle = low + (high - low) / 2;
			mergesort(arr, help, low, middle, asc);
			mergesort(arr, help, middle + 1, high, asc);
			merge(arr, help, low, middle, high, asc);
		}
	}

	private static void merge(String[] arr, String[] help, int low, int middle,
			int high, boolean asc) {
		for (int i = low; i <= high; i++) {
			help[i] = arr[i];
		}
		int i = low;
		int j = middle + 1;
		int k = low;
		while (i <= middle && j <= high) {
			if (asc ? help[i].compareTo(help[j]) < 0 : help[i]
					.compareTo(help[j]) > 0) {
				arr[k] = help[i];
				i++;
			} else {
				arr[k] = help[j];
				j++;
			}
			k++;
		}
		while (i <= middle) {
			arr[k] = help[i];
			k++;
			i++;
		}
	}

	private static void stringWordCheck(String str) {
		if (str == null) {
			throw new IllegalArgumentException("String in the input is null");
		}
		if (!str.matches("[a-zA-Z ]+")) {
			throw new IllegalArgumentException(
					"String must contain letters and spaces");
		}
	}
	
	private static class KeyValuePair implements Comparable<KeyValuePair> {
		char key;
		int value;
		public KeyValuePair(char key, int value) {
			super();
			this.key = key;
			this.value = value;
		}
		@Override
		public int compareTo(KeyValuePair o) {
			return value == o.value ? 
					Character.compare(key, o.key) : o.value - value;
		}
		@Override
		public String toString(){
			return key + ":" + value;
		}
	}
}
