package com.luxoft.dnepr.courses.unit2.model;

public class Circle extends Figure {
	
	public Circle (double radius) {
		super(radius);
	}
	
	@Override
	public double calculateArea(double radius) {
		return Math.PI*Math.pow(radius, 2);
	}

}
