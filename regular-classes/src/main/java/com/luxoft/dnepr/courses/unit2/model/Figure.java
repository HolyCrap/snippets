package com.luxoft.dnepr.courses.unit2.model;

public abstract class Figure {
	private double area;
	public Figure (double d) {
		if (d < 0) {
			throw new IllegalArgumentException("Data must not be below zero");
		}
		this.area = calculateArea(d);
	}
	public abstract double calculateArea(double val);
	public double getArea() {
		return this.area;
	}
}
