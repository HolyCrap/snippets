package com.luxoft.dnepr.courses.unit2.model;

public class Hexagon extends Figure {
	
	public Hexagon(double side) {
		super(side);
	}
	
	@Override
	public double calculateArea(double side) {
		return (3*Math.sqrt(3)/2)*Math.pow(side,2);
	}

}
