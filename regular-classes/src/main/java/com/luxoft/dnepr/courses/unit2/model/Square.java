package com.luxoft.dnepr.courses.unit2.model;

public class Square extends Figure {

	public Square(double side) {
		super(side);
	}

	@Override
	public double calculateArea(double side) {
		return Math.pow(side, 2);
	}

}
