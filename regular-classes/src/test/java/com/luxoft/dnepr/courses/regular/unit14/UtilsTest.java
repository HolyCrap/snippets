package com.luxoft.dnepr.courses.regular.unit14;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class UtilsTest {
	private final String testFileText = "This is a text file. It contains some "
			+ "words. Actually not a lot. That's all.";
	private final String filePath = "test.txt";

	@Test
	public void readTest() throws IOException {
		assertEquals(TextFileUtils.readFile(filePath),testFileText);
	}
	
	@Test
	public void tokenizeTest() {
		String[] tokens = {"this", "text", "file","contains",
				"some", "words","actually", "that's"
		};
		Set<String> expected = new HashSet<>(Arrays.asList(tokens));
		assertEquals(expected, TextFileUtils.tokenizeStringToSet(testFileText));
	}
}
