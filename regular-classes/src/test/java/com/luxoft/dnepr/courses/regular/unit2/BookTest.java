package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();
        assertEquals(book, cloned);
        assertTrue(book.getPublicationDate().equals(cloned.getPublicationDate()));
        assertFalse(book.getPublicationDate()==cloned.getPublicationDate());
        book.setPublicationDate(new GregorianCalendar(2007,0,1).getTime());
        assertNotEquals(book.getPublicationDate(), cloned.getPublicationDate());

    }
    
    @Test
    public void testBookEquals() throws Exception {
        Book book = productFactory.createBook("code", "Book", 45.60, new GregorianCalendar(2007,0,1).getTime());
        Book simpleEquals = book;
        Book complexEquals = productFactory.createBook("code", "Book", 45.60, new GregorianCalendar(2007,0,1).getTime());
        Book priceTest = productFactory.createBook("code", "Book", 35.60, new GregorianCalendar(2007,0,1).getTime());
        assertTrue(book.equals(simpleEquals));
        assertTrue(book.equals(complexEquals));
        assertTrue(book.equals(priceTest));
    }
    
    @Test
    public void testBeverageEqualsAndClone() throws Exception {
    	Beverage beverage = productFactory.createBeverage("code", "name", 20.00, true);
    	Beverage complexEquals = productFactory.createBeverage("code", "name", 20.00, false);
    	assertTrue(!beverage.equals(complexEquals));
    	Beverage cloned = (Beverage) beverage.clone();
    	assertEquals(beverage,cloned);
    	cloned.setNonAlcoholic(false);
    	assertNotEquals(beverage,cloned);
    }
}
