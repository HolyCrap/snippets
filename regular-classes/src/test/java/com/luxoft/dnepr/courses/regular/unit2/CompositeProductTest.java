package com.luxoft.dnepr.courses.regular.unit2;

import java.util.GregorianCalendar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompositeProductTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAdd() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(1, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(2, compositeProduct.getAmount());
    }

    @Test
    public void testParams() {
    	CompositeProduct compositeProduct = new CompositeProduct();
    	compositeProduct.add(factory.createBook("hst", "Horstmann", 100, 
    			new GregorianCalendar(2007,01,01).getTime()));
    	compositeProduct.add(factory.createBook("hst", "Horstmann", 90, 
    			new GregorianCalendar(2007,01,01).getTime()));
    	assertEquals("hst", compositeProduct.getCode());
    	assertEquals("hst", compositeProduct.getChild().getCode());
    	assertEquals("Horstmann", compositeProduct.getName());
    	assertEquals("Horstmann", compositeProduct.getChild().getName());
    }
    @Test
    public void testGetPrice() {
        CompositeProduct compositeProduct = new CompositeProduct();

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(10, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals((10 + 10)* 0.95, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 + 20) * 0.9, compositeProduct.getPrice(), 0);
    }
}
