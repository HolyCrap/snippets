package com.luxoft.dnepr.courses.regular.unit3;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import org.junit.Test;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;

public class BankTest {
	static Bank bank = new Bank("1.7.0");
	{
		userCreator(123, "Anton", 150, 300, WalletStatus.ACTIVE);
		userCreator(124, "Alexander", 30, 299.99, WalletStatus.ACTIVE);
		userCreator(134, "Bogdan", 200, 500, WalletStatus.ACTIVE);
		userCreator(135, "Vitaliy", 43.50, 199.99, WalletStatus.ACTIVE);
	}
	

	@Test(expected = IllegalJavaVersionError.class)
	public void creationErrorTest() {
		new Bank("1.6.0_35");
	}

	@Test
	public void MoneyTransactionCorrectTest() {		
		try {
			bank.makeMoneyTransaction(new Long(123), new Long(124),
					new BigDecimal(100));
			bank.makeMoneyTransaction(new Long(134), new Long(135),
					new BigDecimal(35.50));
		} catch (NoUserFoundException e) {
			e.printStackTrace();
		} catch (TransactionException e) {
			e.printStackTrace();
		}
		assertEquals("50.00",getAmount(123));
		assertEquals("130.00", getAmount(124));
		assertEquals("164.50", getAmount(134));
		assertEquals("79.00",getAmount(135));
		try {
			bank.makeMoneyTransaction(new Long(134), new Long(123),
					new BigDecimal("35.673"));
		} catch (NoUserFoundException e) {
			e.printStackTrace();
		} catch (TransactionException e) {
			e.printStackTrace();
		}
		assertEquals("128.83", getAmount(134));
		assertEquals("85.67", getAmount(123));
	}

	@Test
	public void NoUserFoundTest() {
		try {
			bank.makeMoneyTransaction(new Long(126), new Long(123),
					new BigDecimal(100));
			fail();
		} catch (NoUserFoundException e) {
			assertEquals("User with ID'126' was not found", e.getMessage());
		} catch (TransactionException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void NoUserFoundTest2() {
		try {
			bank.makeMoneyTransaction(new Long(123), new Long(126),
					new BigDecimal(100));
			fail();
		} catch (NoUserFoundException e) {
			assertEquals("User with ID'126' was not found", e.getMessage());
		} catch (TransactionException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void WalletIsBlockedTest() {
		bank.getUsers().get(new Long(123)).getWallet().setStatus(WalletStatus.BLOCKED);
		try {
			bank.makeMoneyTransaction(new Long(123), new Long(135), new BigDecimal(30));
			fail();
		} catch (NoUserFoundException e) {
			e.printStackTrace();
		} catch (TransactionException e) {
			assertEquals("User 'Anton' wallet is blocked", e.getMessage());
		}
	}

	@Test
	public void WalletIsBlockedTest2() {
		BigDecimal temp = bank.getUsers().get(new Long(123)).getWallet().getAmount();
		bank.getUsers().get(new Long(123)).getWallet().setStatus(WalletStatus.ACTIVE);
		bank.getUsers().get(new Long(123)).getWallet().setAmount(new BigDecimal(1));
		WalletInterface experimental = bank.getUsers().get(new Long(135)).getWallet();
		experimental.setStatus(WalletStatus.BLOCKED);
		//for WalletIsBlockedException priority testing, actually later
		//will invoke WalletLimitExceededException
		experimental.setMaxAmount(new BigDecimal(5));
		experimental.setAmount(new BigDecimal(1));
		try {
			bank.makeMoneyTransaction(new Long(123), new Long(135), new BigDecimal(30));
			fail();
		} catch (NoUserFoundException e) {
			e.printStackTrace();
		} catch (TransactionException e) {
			assertEquals("User 'Vitaliy' wallet is blocked", e.getMessage());
			bank.getUsers().get(new Long(123)).getWallet().setAmount(temp);
		}
	}
	
	@Test
	public void WalletLimitExceededTest() {
		bank.getUsers().get(new Long(135)).getWallet().setStatus(WalletStatus.ACTIVE);
		WalletInterface experimental = bank.getUsers().get(new Long(135)).getWallet();
		experimental.setMaxAmount(new BigDecimal(5));
		experimental.setAmount(new BigDecimal(1));
		try {
			bank.makeMoneyTransaction(new Long(123), new Long(135), new BigDecimal(30.35));
			fail();
		} catch (NoUserFoundException e) {
			e.printStackTrace();
		} catch (TransactionException e) {
			assertEquals("User 'Vitaliy' wallet limit exceeded (1.00 + 30.35 > 5.00)", e.getMessage());
		}
	}
	
	@Test
	public void WalletLimitExceededTest2() {
		WalletInterface from = bank.getUsers().get(new Long(123)).getWallet();
		WalletInterface to = bank.getUsers().get(new Long(124)).getWallet();
		from.setAmount(new BigDecimal(300));
		to.setMaxAmount(new BigDecimal(299.99));
		to.setAmount(new BigDecimal(0));
		try {
			bank.makeMoneyTransaction(new Long(123), new Long(124), new BigDecimal(299.999));
			fail();
		} catch (NoUserFoundException e) {
			e.printStackTrace();
		} catch (TransactionException e) {
			assertEquals("User 'Alexander' wallet limit exceeded (0.00 + 300.00 > 299.99)", e.getMessage());
		}
	}
	
	@Test
	public void InsufficientFundsTest() {
		bank.getUsers().get(new Long(123)).getWallet().setAmount(new BigDecimal(1.3));
		try {
			bank.makeMoneyTransaction(new Long(123), new Long(135), new BigDecimal(30.35));
			fail();
		} catch (NoUserFoundException e) {
			e.printStackTrace();
		} catch (TransactionException e) {
			assertEquals("User 'Anton' has insufficient funds (1.30 < 30.35)", e.getMessage());
		}
	}
	
	@Test
	public void InsufficientFundsTest2() {
		try {
			bank.makeMoneyTransaction(new Long(135), new Long(123), new BigDecimal(43.501));
			fail();
		} catch (NoUserFoundException e) {
			e.printStackTrace();
		} catch (TransactionException e) {
			assertEquals("User 'Vitaliy' has insufficient funds (43.50 < 43.50)", e.getMessage());
		}
	}
	
	@Test
	public void correctMoneyTransaction() {
		try {
			bank.makeMoneyTransaction(new Long(135), new Long(123), new BigDecimal(43.49999));
			assertEquals(new BigDecimal(0.00001).setScale(5,BigDecimal.ROUND_FLOOR),
					bank.getUsers().get(new Long(135)).
					getWallet().getAmount().setScale(5, BigDecimal.ROUND_FLOOR));
		} catch (NoUserFoundException e) {
			fail();
		} catch (TransactionException e) {
			fail();
		}
	}
	
	private void userCreator(int id, String name, double amount,
			double maxAmount, WalletStatus status) {
		bank.getUsers().put(new Long(id), new User(new Long(id), name, new Wallet(
				id * 10, new BigDecimal(amount), status, new BigDecimal(maxAmount))));
	}
	
	private String getAmount(int id) {
		return BankUtils.decimalOut(bank.getUsers().get(new Long(id)).getWallet().getAmount());
	}
}
