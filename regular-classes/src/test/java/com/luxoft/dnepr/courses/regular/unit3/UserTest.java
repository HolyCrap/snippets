package com.luxoft.dnepr.courses.regular.unit3;

import static org.junit.Assert.*;

import java.util.InputMismatchException;

import org.junit.Test;

public class UserTest {
	
	@Test
	public void creationErrorTest() {
		try {
			new User(null," ",null);
		} catch (InputMismatchException e) {
			assertEquals("Id is null", e.getMessage());
		}
		try {
			new User(new Long(322)," ", null);
		} catch (InputMismatchException e) {
			assertEquals("Wallet is null", e.getMessage());
		}
		try {
			new User(new Long(322), null, null);
		} catch (InputMismatchException e) {
			assertEquals("Name is null", e.getMessage());
		}
	}
}
