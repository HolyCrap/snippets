package com.luxoft.dnepr.courses.regular.unit3;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.InputMismatchException;

import org.junit.Test;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

public class WalletTest {
	private Wallet wallet = new Wallet(314, new BigDecimal(100),
			WalletStatus.ACTIVE, new BigDecimal(150));

	@Test
	public void creationTest() {
		try {
			new Wallet(314, null, WalletStatus.ACTIVE, new BigDecimal(150));
		} catch (InputMismatchException e) {
			assertEquals("Amount is null", e.getMessage());
		}
		try {
			new Wallet(314, new BigDecimal(30), null, new BigDecimal(150));
		} catch (InputMismatchException e) {
			assertEquals("Status is null", e.getMessage());
		}
		try {
			new Wallet(314, new BigDecimal(30), WalletStatus.ACTIVE, null);
		} catch (InputMismatchException e) {
			assertEquals("Max amount is null", e.getMessage());
		}
	}

	@Test(expected = InsufficientWalletAmountException.class)
	public void checkWithdrawalTest() throws WalletIsBlockedException,
			InsufficientWalletAmountException {
		wallet.setStatus(WalletStatus.ACTIVE);
		wallet.checkWithdrawal(new BigDecimal(120));
	}

	@Test(expected = WalletIsBlockedException.class)
	public void checkWithdrawalTest2() throws WalletIsBlockedException,
			InsufficientWalletAmountException {
		wallet.setStatus(WalletStatus.BLOCKED);
		wallet.checkWithdrawal(new BigDecimal(120));
	}

	@Test(expected = WalletIsBlockedException.class)
	public void checkTransferTest() throws WalletIsBlockedException, 
			LimitExceededException {
		wallet.setStatus(WalletStatus.BLOCKED);
		wallet.checkTransfer(new BigDecimal(100));
	}
	
	@Test(expected = LimitExceededException.class)
	public void checkTransferTest2() throws WalletIsBlockedException, 
			LimitExceededException {
		wallet.setStatus(WalletStatus.ACTIVE);
		wallet.checkTransfer(new BigDecimal(100));
	}
}
