package com.luxoft.dnepr.courses.regular.unit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

import org.junit.*;

import static org.junit.Assert.*;

public class EqualSetTest {
	private EqualSet<Integer> testSet;
	private ArrayList<Integer> testArray;
	
	@Before
	public void initEqualSet() {
		//method add() and default constructor
		testArray = new ArrayList<Integer>();
		testArray.add(Integer.valueOf(21));
		testArray.add(null);
		testArray.add(Integer.valueOf(14));
		testArray.add(null);
		testArray.add(Integer.valueOf(14));
		testSet = new EqualSet<Integer>();
		testSet.add(Integer.valueOf(21));
		testSet.add(null);
		testSet.add(Integer.valueOf(14));
		testSet.add(null);
		testSet.add(Integer.valueOf(14));
	}
	
	@Test
	public void initTest1() {
		//size() and add()
		assertEquals(3,testSet.size());
		assertEquals(5,testArray.size());
		assertTrue(testSet.add(new Integer(152)));
		assertFalse(testSet.add(null));
		assertFalse(testSet.add(Integer.valueOf(14)));
	}
	
	@Test
	public void initTest2() {
		//clear(), contains() and specific constructor
		testSet.clear();
		testSet.addAll(testArray);
		assertEquals(3,testSet.size());
		assertEquals(5,testArray.size());
		for (Integer elem: testSet) {
			assertTrue(testArray.contains(elem));
		}
		for (Integer elem: testArray) {
			assertTrue(testSet.contains(elem));
		}
		assertTrue(testSet.contains(null));
		assertFalse(testSet.contains(Integer.valueOf(156)));
	}	

	@Test
	public void containsAllTest() {
		assertTrue(testSet.containsAll(testArray));
		testArray.add(Integer.valueOf(51));
		assertFalse(testSet.containsAll(testArray));
	}
	
	@Test
	public void addAllTest() {
		assertFalse(testSet.addAll(testArray));
		testArray.add(Integer.valueOf(15));
		assertTrue(testSet.addAll(testArray));
	}
	@Test
	public void isEmptyTest() {
		assertFalse(testSet.isEmpty());
		testSet.clear();
		assertTrue(testSet.isEmpty());
	}
	
	@Test
	public void iteratorTest() {
		Iterator<Integer> it = testSet.iterator();
		int counter = 0;
		while(it.hasNext()) {
			Integer val = it.next();
			assertTrue(val == null ||
					val.equals(Integer.valueOf(14)) ||
					val.equals(Integer.valueOf(21)));
			counter++;
		}
		assertEquals(3,counter);
	}
	
	@Test
	public void removeAndRemoveAlltest() {
		ArrayList<Integer> testArray2 = new ArrayList<Integer>();
		testArray2.add(Integer.valueOf(15));
		testArray2.add(Integer.valueOf(156));
		testArray2.add(Integer.valueOf(91));
		assertFalse(testSet.remove(Integer.valueOf(151)));
		assertTrue(testSet.remove(null));
		assertFalse(testSet.remove(null));
		assertTrue(testSet.removeAll(testArray));
		assertTrue(testSet.isEmpty());
		assertFalse(testSet.removeAll(testArray2));
		testSet.addAll(testArray);
		testArray.remove(Integer.valueOf(21));
		assertTrue(testSet.removeAll(testArray));
		testSet.addAll(testArray);
		assertFalse(testSet.removeAll(testArray2));
		testArray2.add(Integer.valueOf(21));
		assertTrue(testSet.retainAll(testArray2));
	}
	
	@Test
	public void retainAllTest() {
		ArrayList<Integer> testArray2 = new ArrayList<Integer>();
		testArray2.add(Integer.valueOf(21));
		testArray2.add(Integer.valueOf(14));
		testArray2.add(Integer.valueOf(21));
		testArray2.add(Integer.valueOf(21));
		assertTrue(testSet.retainAll(testArray2));
		assertFalse(testSet.contains(null));
		assertTrue(testSet.contains(Integer.valueOf(21)));
		assertEquals(2,testSet.size());
	}
	
	@Test
	public void toArrayTest() {
		testSet.remove(null);
		testSet.add(Integer.valueOf(17));
		Object[] array = testSet.toArray();
		assertEquals(array.length,3);
		Arrays.sort(array);
		assertArrayEquals(new Object[]{Integer.valueOf(14), 
				Integer.valueOf(17), Integer.valueOf(21)}, array);
		Integer[] array2 = testSet.toArray(new Integer[0]);
		Arrays.sort(array2);
		assertArrayEquals(new Object[]{Integer.valueOf(14), 
				Integer.valueOf(17), Integer.valueOf(21)}, array);
	}
	
	@Test (expected = NullPointerException.class)
	public void exceptionTest1(){
		testSet.addAll(null);
	}
	
	@Test (expected = NullPointerException.class)
	public void exceptionTest2(){
		testSet.removeAll(null);
	}
	
	@Test (expected = NullPointerException.class)
	public void exceptionTest3(){
		testSet.retainAll(null);
	}
	
	@Test (expected = NullPointerException.class)
	public void exceptionTest4(){
		testSet.containsAll(null);
	}
	
	@Test
	public void equalsAndHashcodeTest() {
		EqualSet<Integer> newSet = new EqualSet<>();
		newSet.add(Integer.valueOf(14));
		newSet.add(Integer.valueOf(21));
		newSet.add(null);
		assertTrue(testSet.equals(newSet));
		assertEquals(testSet.hashCode(), newSet.hashCode());
		HashSet<Integer> hashSet = new HashSet<>(newSet);
		assertTrue(testSet.equals(hashSet));
		assertEquals(testSet.hashCode(), hashSet.hashCode());
		newSet.add(Integer.valueOf(15));
		assertFalse(testSet.equals(newSet));
		newSet.remove(Integer.valueOf(15));
		newSet.remove(Integer.valueOf(14));
		assertFalse(testSet.equals(newSet));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void cloneTest() {
		EqualSet<Integer> clone = (EqualSet<Integer>) testSet.clone();
		assertTrue(clone.equals(testSet));
		clone.add(Integer.valueOf(151));
		assertFalse(clone.equals(testSet));
	}
	
    @Test
    public void testConstructor() {
        EqualSet<Integer> set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8}));
        Assert.assertEquals(8, set.size());

        set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 1, 1, 2, 2}));
        Assert.assertEquals(2, set.size());
    }

    @Test
    public void testConstructorWithNulls() {
        EqualSet<Integer> set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, null}));
        Assert.assertEquals(9, set.size());

        set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 1, 1, 2, 2, null, null, null, null}));
        Assert.assertEquals(3, set.size());
    }
}
