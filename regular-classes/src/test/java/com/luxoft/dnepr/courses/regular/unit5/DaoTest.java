package com.luxoft.dnepr.courses.regular.unit5;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.*;

import com.luxoft.dnepr.courses.regular.unit5.dao.*;
import com.luxoft.dnepr.courses.regular.unit5.exception.*;
import com.luxoft.dnepr.courses.regular.unit5.model.*;
import com.luxoft.dnepr.courses.regular.unit5.storage.StorageHolder;

public class DaoTest {
	private static AbstractDao<Redis> daoRedis = new AbstractDao<Redis>(
			StorageHolder.getRedisStorage());
	private static AbstractDao<Employee> daoEmpl = new AbstractDao<Employee>(
			StorageHolder.getEmployeeStorage());
	private static Map<Long, Redis> redis = StorageHolder.getRedisStorage()
			.getEntities();
	private static Map<Long, Employee> empl = StorageHolder
			.getEmployeeStorage().getEntities();

	@Before
	public void init() {
		redis.clear();
		empl.clear();
		redis.put(Long.valueOf(1),
				EntityFactory.createRedis(Long.valueOf(1), 14));
		empl.put(Long.valueOf(14),
				EntityFactory.createEmployee(Long.valueOf(14), 4000));
		redis.put(Long.valueOf(2),
				EntityFactory.createRedis(Long.valueOf(2), 15));
		empl.put(Long.valueOf(15),
				EntityFactory.createEmployee(Long.valueOf(15), 3500));
	}

	@Test
	public void saveTest() {
		daoRedis.save(EntityFactory.createRedis(Long.valueOf(4), 10));
		daoEmpl.save(EntityFactory.createEmployee(Long.valueOf(13), 4500));
		assertTrue(redis.containsKey(Long.valueOf(4)));
		assertEquals(10, redis.get(Long.valueOf(4)).getWeight());
		assertTrue(empl.containsKey(Long.valueOf(13)));
		assertEquals(4500, empl.get(Long.valueOf(13)).getSalary());
	}

	@Test
	public void nullSaveTest() {
		daoEmpl.save(EntityFactory.createEmployee(null, 15000));
		daoRedis.save(EntityFactory.createRedis(null, 151));
		assertTrue(redis.containsKey(Long.valueOf(3)));
		assertTrue(empl.containsKey(Long.valueOf(16)));
		assertEquals(15000, empl.get(Long.valueOf(16)).getSalary());
		assertEquals(151, redis.get(Long.valueOf(3)).getWeight());
	}

	@Test(expected = UserAlreadyExist.class)
	public void userAlreadyExists() {
		daoEmpl.save(EntityFactory.createEmployee(Long.valueOf(14), 1000));
	}

	@Test(expected = UserAlreadyExist.class)
	public void userAlreadyExists2() throws UserAlreadyExist {
		daoRedis.save(EntityFactory.createRedis(Long.valueOf(2), 1000));
	}

	@Test
	public void updateCorrectTest() {
		daoEmpl.update(EntityFactory.createEmployee(Long.valueOf(14), 14));
		assertEquals(14, empl.get(Long.valueOf(14)).getSalary());
		daoEmpl.update(EntityFactory.createEmployee(Long.valueOf(14), 4000));
		assertEquals(4000, empl.get(Long.valueOf(14)).getSalary());
	}

	@Test(expected = UserNotFound.class)
	public void updateExceptionTest1() {
		daoEmpl.update(EntityFactory.createEmployee(Long.valueOf(10), 10000));
	}

	@Test(expected = UserNotFound.class)
	public void updateExceptionTest2() {
		daoEmpl.update(EntityFactory.createEmployee(null, 10000));
	}

	@Test
	public void getCorrectTest() {
		Employee got = daoEmpl.get(14);
		assertEquals(got.getId(), Long.valueOf(14));
		assertEquals(got.getSalary(), 4000);
		Redis got2 = daoRedis.get(2);
		assertEquals(got2.getId(), Long.valueOf(2));
		assertEquals(got2.getWeight(), 15);
	}

	@Test
	public void getNotFoundTest() {
		Employee got = daoEmpl.get(10);
		assertEquals(got, null);
		Redis got2 = daoRedis.get(3);
		assertEquals(got2, null);
	}

	@Test
	public void deleteCorrectTest() {
		assertTrue(daoEmpl.delete(14));
		assertFalse(empl.containsKey(Long.valueOf(14)));
		assertTrue(daoRedis.delete(2));
		assertFalse(redis.containsKey(2));
		assertFalse(daoEmpl.delete(14));
		assertFalse(daoRedis.delete(14));
	}
}
