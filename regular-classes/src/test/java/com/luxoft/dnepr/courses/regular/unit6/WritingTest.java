package com.luxoft.dnepr.courses.regular.unit6;

import static org.junit.Assert.*;

import java.io.*;

import org.junit.*;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.*;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;

public class WritingTest {
	private static final String PATH = "E:\\test.txt";
	private static final String GALJA = "{\"name\":\"Galja\",\"gender\":"
			+ "\"FEMALE\",\"ethnicity\""
    		+ ":\"ukrainian\",\"age\":20";
	private static final String FATHER = "{\"name\":\"Vasiliy\",\"gender\":"
			+ "\"MALE\",\"ethnicity\""
    		+ ":\"ukrainian\",\"age\":40";
	private static final String MOTHER = "{\"name\":\"Vera\",\"gender\":"
			+ "\"FEMALE\",\"ethnicity\""
    		+ ":\"greek\",\"age\":38";
	private static final String GRANDFATHER = "{\"name\":\"Did Vasyl\",\"gender\":"
			+ "\"MALE\",\"ethnicity\""
    		+ ":\"mongol\",\"age\":70";
	private static FamilyTree tree;
	
	@Before
	public void init() {
		PersonImpl person = new PersonImpl();
        person.setAge(20);
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        PersonImpl father = new PersonImpl();
        father.setAge(40);
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
        PersonImpl grandFather = new PersonImpl();
        grandFather.setAge(70);
        grandFather.setEthnicity("mongol");
        grandFather.setGender(Gender.MALE);
        grandFather.setName("Did Vasyl");
        father.setFather(grandFather);
        PersonImpl mother = new PersonImpl();
        mother.setAge(38);
        mother.setEthnicity("greek");
        mother.setGender(Gender.FEMALE);
        mother.setName("Vera");
        person.setMother(mother);
        tree = FamilyTreeImpl.create(person);
	}
	@Test
	public void writingTest() throws IOException {	
		try {
			IOUtils.save(PATH, tree);
		} catch (IOException e) {
			e.printStackTrace();
		}
		test();
	}
	
	@Test
	public void writingTest2() throws IOException {
		FileOutputStream fs = new FileOutputStream(new File(PATH));
		IOUtils.save(fs, tree);
		fs.close();
		test();	
	}
	
	private static void test() throws IOException {
		String result = read();
		int index = result.indexOf('{');
		result = result.substring(index, result.length());
        assertEquals("{\"root\":" + GALJA + ",\"father\":" + FATHER + 
        		",\"father\":" + GRANDFATHER + ",\"father\":null,\"mother\":null}" + 
        	     ",\"mother\":null},\"mother\":" + MOTHER + 
        		",\"father\":null,\"mother\":null}}}x",result);
	}

	private static String read() throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(
				new File(PATH)));
		StringBuilder sb = new StringBuilder();
		char[] buffer = new char[30];
		int read = 0;
		while (true) {
			read = reader.read(buffer);
			if (read == -1)
				break;
			sb.append(buffer, 0, read);
		}
		reader.close();
		return sb.toString();
	}
}
