package com.luxoft.dnepr.courses.regular.unit7;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class _WordStorageTest {
    private WordStorage wordStorage;

    @Before
    public void setUp() {
        wordStorage = new WordStorage();
    }

    @After
    public void tearDown() {
        wordStorage = null;
    }

    @Test
    public void testSave() throws Exception {
        wordStorage.save("test");
        wordStorage.save("lalalala");
        wordStorage.save("test");
        wordStorage.save("33333");
        for (int i = 0; i < 10; i++)
        	wordStorage.save("multiply");
        Map<String, ? extends Number> storage = wordStorage.getWordStatistics();
        assertTrue(storage.containsKey("test"));
        assertTrue(storage.containsKey("lalalala"));
        assertTrue(storage.containsKey("33333"));
        assertTrue(storage.containsKey("multiply"));
        assertEquals(2,storage.get("test").intValue());
        assertEquals(1,storage.get("lalalala").intValue());
        assertEquals(1,storage.get("33333").intValue());
        assertEquals(10,storage.get("multiply").intValue());
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void saveExceptionTest() {
    	wordStorage.save(null);
    }

    @Test
    public void testGetWordStatistics() {
        assertEquals(0, wordStorage.getWordStatistics().size());
        wordStorage.save("test");
        assertEquals(1, wordStorage.getWordStatistics().size());

        try {
            wordStorage.getWordStatistics().remove("test");
            fail("wordStatistics isn't unmodifiable");
        } catch (UnsupportedOperationException e) {}
    }

    @Test
    public void testSave2() throws Exception {
        final int numThreads = 100;
        final int numSaves = 10000;
 
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < numThreads; i++) {
            threads.add(new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < numSaves; j++) {
                        wordStorage.save(Integer.toString(j));
                    }
                }
            }));
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
 
        assertEquals(numSaves, wordStorage.getWordStatistics().size());
        for (Number number : wordStorage.getWordStatistics().values()) {
            assertEquals(numThreads, number.intValue());
        }
    }
}
