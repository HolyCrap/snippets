package com.luxoft.dnepr.courses.unit1;

import org.junit.*;

public final class LuxoftUtilsTest {
	
	@Test
	public void getMonthNameTest() {
		
		Assert.assertEquals("February", LuxoftUtils.getMonthName(2, "en"));
		Assert.assertEquals("Июнь", LuxoftUtils.getMonthName(6, "ru"));
		Assert.assertEquals("September", LuxoftUtils.getMonthName(9, "en"));
		Assert.assertEquals("Ноябрь", LuxoftUtils.getMonthName(11, "ru"));
		Assert.assertEquals("March", LuxoftUtils.getMonthName(3, "en"));

		Assert.assertEquals("Unknown Month", LuxoftUtils.getMonthName(0, "en"));
		Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName(0, "ru"));
		Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(0, "12"));
		Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(12, null));
	}
	
	@Test
	public void binaryToDecimalTest() {
		
		Assert.assertEquals("12", LuxoftUtils.binaryToDecimal("1100"));
		Assert.assertEquals("6974", LuxoftUtils.binaryToDecimal("1101100111110"));
		Assert.assertEquals("35", LuxoftUtils.binaryToDecimal("100011"));
		
		Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("lalala"));
		Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("35"));
	}
	
	@Test
	public void decimalToBinaryTest() {
		
		Assert.assertEquals("1100", LuxoftUtils.decimalToBinary("12"));
		Assert.assertEquals("1101100111110", LuxoftUtils.decimalToBinary("6974"));
		Assert.assertEquals("100011", LuxoftUtils.decimalToBinary("35"));
		
		Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("18B35AC"));
	}
	
	@Test
	public void sortArrayTest() {

		Assert.assertArrayEquals(new int[] { 1, 2, 3 },
				LuxoftUtils.sortArray(new int[] { 3, 1, 2 }, true));
		Assert.assertArrayEquals(new int[] { 1, 3, 5, 7, 9 },
				LuxoftUtils.sortArray(new int[] { 5, 9, 1, 7, 3 }, true));
		Assert.assertArrayEquals(new int[] { 9, 7, 5, 3, 1 },
				LuxoftUtils.sortArray(new int[] { 5, 9, 1, 7, 3 }, false));
		Assert.assertArrayEquals(
				new int[] { 10, 9, 8, 7, 6, 6, 5, 4, 3, 2, 1 },
				LuxoftUtils.sortArray(new int[] { 5, 2, 3, 6, 1, 7, 9, 10, 6,
						4, 8 }, false));
	}
}
