package com.luxoft.dnepr.courses.unit2;

import java.util.ArrayList;
import org.junit.*;
import com.luxoft.dnepr.courses.unit2.model.*;

public final class LuxoftUtilsTest {

	@Test
	public void sortArrayCorrectTest() {
		ArrayList<String[]> tests = new ArrayList<String[]>(2);
		ArrayList<String[]> expTrues = new ArrayList<String[]>(2);
		ArrayList<String[]> expFalses = new ArrayList<String[]>(2);
		tests.add(new String[] { "b", "d", "c", "a", "e", "Z", "A" });
		expTrues.add(new String[] { "A", "Z", "a", "b", "c", "d", "e" });
		expFalses.add(new String[] { "e", "d", "c", "b", "a", "Z", "A" });
		tests.add(new String[] { "Kate", "Steve", "Stanley", "Adam", "steve",
				"scott" });
		expTrues.add(new String[] { "Adam", "Kate", "Stanley", "Steve",
				"scott", "steve" });
		expFalses.add(new String[] { "steve", "scott", "Steve", "Stanley",
				"Kate", "Adam" });
		tests.add(new String[] { "Kate A", "Steve B", "Stanley", "Adam",
				"steve", "scott" });
		expTrues.add(new String[] { "Adam", "Kate A", "Stanley", "Steve B",
				"scott", "steve" });
		expFalses.add(new String[] { "steve", "scott", "Steve B", "Stanley",
				"Kate A", "Adam" });
		for (int i = 0; i < 3; i++) {
			Assert.assertArrayEquals(expTrues.get(i),
					LuxoftUtils.sortArray(tests.get(i), true));
			Assert.assertArrayEquals(expFalses.get(i),
					LuxoftUtils.sortArray(tests.get(i), false));
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void sortArrayIllegalArgumentExceptionTest() {
		LuxoftUtils.sortArray(new String[] { "adam", "a0dt", "as03" }, true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void sortArrayIllegalArgumentExceptionTest2() {
		LuxoftUtils.sortArray(new String[] { null, "a", "ab" }, true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void sortArrayIllegalArgumentExceptionTest3() {
		LuxoftUtils.sortArray(null, true);
	}

	@Test
	public void wordAverageLengthCorrectTest() {
		Assert.assertEquals(2.25,
				LuxoftUtils.wordAverageLength("I have a cat"), 0.000001);
		Assert.assertEquals(2.25,
				LuxoftUtils.wordAverageLength("I have a cat "), 0.000001);
		Assert.assertEquals(3.14285714,
				LuxoftUtils.wordAverageLength("Here is the story of my life"),
				0.000001);
		Assert.assertEquals(3.14285714,
				LuxoftUtils.wordAverageLength("Here is the story of my life "),
				0.000001);
		Assert.assertEquals(4.4,
				LuxoftUtils.wordAverageLength("Comparing a lot of strings"),
				0.000001);
		Assert.assertEquals(2.25,
				LuxoftUtils.wordAverageLength("I    have a cat"), 0.000001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void wordAverageLengthExceptionTest() {
		LuxoftUtils.wordAverageLength("I have 12 shoes");
	}

	@Test(expected = IllegalArgumentException.class)
	public void wordAverageLengthExceptionTest2() {
		LuxoftUtils.wordAverageLength(null);
	}
	@Test
	public void reverseWordsCorrectTest() {
		Assert.assertEquals("I evah a tac",
				LuxoftUtils.reverseWords("I have a cat"));
		Assert.assertEquals("I evah a tac ",
				LuxoftUtils.reverseWords("I have a cat "));
		Assert.assertEquals("I   evah a tac",
				LuxoftUtils.reverseWords("I   have a cat"));
		Assert.assertEquals("ereH si eht yrots fo ym efil",
				LuxoftUtils.reverseWords("Here is the story of my life"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void reverseWordsExceptionTest() {
		LuxoftUtils.reverseWords("I have 12 shoes");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void reverseWordsExceptionTest2() {
		LuxoftUtils.reverseWords(null);
	}
	
	@Test
	public void getCharEntriesCorrectTest() {
		Assert.assertArrayEquals(new char[]{'a', 'I', 'c','e','h','t', 'v'}, 
				LuxoftUtils.getCharEntries("I have a cat"));
		Assert.assertArrayEquals(new char[]{'o','a', 'g', 'i', 'n', 'r', 's', 't', 'C','f','l', 'm', 'p'}, 
				LuxoftUtils.getCharEntries("Comparing a lot of strings"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getCharEntriesExceptionTest1() {
		LuxoftUtils.getCharEntries("I have 12 grn");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getCharEntriesExceptionTest2() {
		LuxoftUtils.getCharEntries(null);
	}
	
	@Test
	public void calculateOverallAreaCorrectTest() {
		ArrayList<Figure> list = new ArrayList<Figure>();
		list.add(new Circle(2));
		list.add(new Hexagon(3));
		list.add(new Square(4));
		Assert.assertEquals(51.9490565161, LuxoftUtils.calculateOverallArea(list), 0.000001);
		list.clear();
		list.add(new Circle(3.5));
		list.add(new Hexagon(7.3));
		list.add(new Square(10.8));
		Assert.assertEquals(293.575991289, LuxoftUtils.calculateOverallArea(list), 0.000001);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void calculateOverallAreaExceptionTest() {
		ArrayList<Figure> list = new ArrayList<Figure>();
		list.add(new Circle(-4));
		LuxoftUtils.calculateOverallArea(list);
	}
	
}
