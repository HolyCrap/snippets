CREATE TABLE IF NOT EXISTS makers (
	maker_id INT NOT NULL,
	maker_name VARCHAR(50) NOT NULL,
	maker_adress VARCHAR(200),
	CONSTRAINT pk_makers_makerid PRIMARY KEY(maker_id)
);

INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (1, 'A', 'AdressA');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (2, 'B', 'AdressB');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (3, 'C', 'AdressC');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (4, 'D', 'AdressD');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (5, 'E', 'AdressE');

ALTER TABLE product ADD COLUMN maker_id INT NOT NULL;
UPDATE product
SET maker_id = (
	SELECT m.maker_id
	FROM makers m
	WHERE m.maker_name = product.maker);
ALTER TABLE product ADD CONSTRAINT fk_product_makers FOREIGN KEY(maker_id) REFERENCES makers(maker_id);
ALTER TABLE product DROP COLUMN maker;

CREATE TABLE IF NOT EXISTS printer_type (
	type_id INT NOT NULL PRIMARY KEY DEFAULT '0',
	type_name VARCHAR(50) NOT NULL
);

INSERT INTO printer_type(type_id, type_name) VALUES (1, 'Laser');
INSERT INTO printer_type(type_id, type_name) VALUES (2, 'Jet');
INSERT INTO printer_type(type_id, type_name) VALUES (3, 'Matrix');

ALTER TABLE printer ADD COLUMN type_id INT NOT NULL;
UPDATE printer
SET type_id = (
	SELECT type_id
	FROM  printer_type pt
	WHERE pt.type_name = printer.type);
ALTER TABLE printer ADD CONSTRAINT fk_printer_type FOREIGN KEY(type_id) REFERENCES printer_type(type_id);
ALTER TABLE printer DROP COLUMN type;
ALTER TABLE printer MODIFY COLUMN color CHAR NOT NULL DEFAULT 'y';

CREATE INDEX ind_pc_price ON pc(price ASC);
CREATE INDEX ind_laptop_price ON laptop(price ASC);
CREATE INDEX ind_printer_price ON printer(price ASC);