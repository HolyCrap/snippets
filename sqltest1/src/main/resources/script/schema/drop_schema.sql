DROP INDEX ind_pc_price ON pc;
DROP INDEX ind_laptop_price ON laptop;
DROP INDEX ind_printer_price ON printer;

ALTER TABLE product DROP FOREIGN KEY fk_product_makers;
ALTER TABLE printer DROP FOREIGN KEY fk_printer_type;
ALTER TABLE laptop DROP FOREIGN KEY fk_laptop_product;
ALTER TABLE pc DROP FOREIGN KEY fk_pc_product;
ALTER TABLE printer DROP FOREIGN KEY fk_printer_product;

TRUNCATE TABLE laptop;
TRUNCATE TABLE pc;
TRUNCATE TABLE makers;
TRUNCATE TABLE printer;
TRUNCATE TABLE printer_type;
TRUNCATE TABLE product;

DROP TABLE laptop;
DROP TABLE pc;
DROP TABLE makers;
DROP TABLE printer;
DROP TABLE printer_type;
DROP TABLE product;