SELECT AVG(speed) as avg_speed
FROM pc
WHERE model IN (
	SELECT p.model 
	FROM product p INNER JOIN makers m
	ON m.maker_id = p.maker_id
	WHERE maker_name = 'A'
)