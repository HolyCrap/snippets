SELECT DISTINCT maker_id, type
FROM product
WHERE maker_id IN (
	SELECT maker_id
	FROM product
	GROUP BY maker_id
	HAVING COUNT(DISTINCT model) > 1
		AND 
		COUNT(DISTINCT type) = 1
)