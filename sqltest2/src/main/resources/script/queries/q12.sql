SELECT DISTINCT hd
FROM pc
GROUP BY hd
HAVING COUNT(DISTINCT id) > 1
ORDER BY hd DESC