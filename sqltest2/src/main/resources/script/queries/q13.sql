SELECT p.type, p.model, l.speed
FROM laptop l INNER JOIN product p
ON l.model = p.model
WHERE speed < ALL (
	SELECT speed
	FROM pc
)