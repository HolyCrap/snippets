SELECT m.maker_name, pr.price
FROM makers m
INNER JOIN product p
ON m.maker_id = p.maker_id
INNER JOIN printer pr
ON p.model = pr.model
WHERE pr.color = 'y' AND pr.price = (
	SELECT MIN(price)
	FROM printer
	GROUP BY color
	HAVING color = 'y'
)