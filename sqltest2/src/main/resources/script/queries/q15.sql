SELECT p.maker_id, AVG(screen) avg_size
FROM product p INNER JOIN laptop l
ON p.model = l.model
GROUP BY p.maker_id
ORDER BY avg_size 