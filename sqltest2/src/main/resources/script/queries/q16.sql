SELECT maker_id, COUNT(DISTINCT model) mod_count
FROM product
WHERE type = 'PC'
GROUP BY maker_id
HAVING mod_count >=3
