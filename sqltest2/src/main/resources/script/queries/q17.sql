SELECT DISTINCT maker_id
FROM product
WHERE type = 'Printer' and maker_id IN (
	SELECT p.maker_id
	FROM product p INNER JOIN pc
	ON p.model = pc.model
	WHERE ram = (
		SELECT MIN(ram)
		FROM pc)
	AND speed = (
		SELECT MAX(speed)
		FROM pc
		WHERE ram = (
			SELECT MIN(ram)
			FROM pc)
		)
)