SELECT model
FROM pc
WHERE price = (
	SELECT MAX(pr)
	FROM (
		SELECT MAX(price) as pr
		FROM pc
		UNION ALL 
		SELECT MAX(price) as pr
		FROM printer
		UNION ALL
		SELECT MAX(price) as pr
		FROM laptop
	) as t
)
UNION
SELECT model
FROM laptop
WHERE price = (
	SELECT MAX(pr)
	FROM (
		SELECT MAX(price) as pr
		FROM pc
		UNION ALL 
		SELECT MAX(price) as pr
		FROM printer
		UNION ALL
		SELECT MAX(price) as pr
		FROM laptop
	) as t
)
UNION
SELECT model
FROM printer
WHERE price = (
	SELECT MAX(pr)
	FROM (
		SELECT MAX(price) as pr
		FROM pc
		UNION ALL 
		SELECT MAX(price) as pr
		FROM printer
		UNION ALL
		SELECT MAX(price) as pr
		FROM laptop
	) as t
)