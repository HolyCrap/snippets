SELECT maker_id, AVG(hd) avg_hd
FROM pc INNER JOIN product
ON pc.model = product.model
WHERE maker_id IN (
	SELECT maker_id
	FROM product 
	WHERE type = 'Printer'
)
GROUP BY product.maker_id
