SELECT m.maker_name, l.speed
FROM laptop l INNER JOIN product p
ON l.model = p.model
INNER JOIN makers m
ON m.maker_id = p.maker_id
WHERE l.hd >= 10;