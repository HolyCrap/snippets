SELECT AVG(hd) AS avg_hd
FROM pc
WHERE model IN (
	SELECT model 
	FROM product
	WHERE type = 'PC' AND maker_id IN (
		SELECT maker_id	
		FROM product
		WHERE type = 'Printer'
	)
)
