SELECT DISTINCT m.maker_name
FROM makers m INNER JOIN product p
ON m.maker_id = p.maker_id
WHERE p.type = 'Printer'
ORDER BY maker_name DESC;