SELECT p.model, t.price
FROM product p INNER JOIN makers m
ON p.maker_id = m.maker_id
INNER JOIN laptop t
ON t.model = p.model
WHERE maker_name = 'B'
UNION
SELECT p.model, t.price
FROM product p INNER JOIN makers m
ON p.maker_id = m.maker_id
INNER JOIN pc t
ON t.model = p.model
WHERE maker_name = 'B'
UNION
SELECT p.model, t.price
FROM product p INNER JOIN makers m
ON p.maker_id = m.maker_id
INNER JOIN printer t
ON t.model = p.model
WHERE maker_name = 'B'