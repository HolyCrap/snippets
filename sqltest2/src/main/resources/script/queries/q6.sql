SELECT maker_name
FROM makers
WHERE maker_id IN (
	SELECT maker_id
	FROM product
	WHERE type = 'PC' AND maker_id NOT IN (
		SELECT maker_id
		FROM product
		WHERE type = 'Laptop'
	)
)