SELECT maker_name
FROM makers
WHERE maker_id IN (
	SELECT maker_id
	FROM product
	WHERE model IN (
		SELECT model
		FROM pc
		WHERE speed >= 450
	)
)